package pl.pol.michal.tictactoeapp.config.exception;

public class Conflict extends Exception {
    public Conflict(String message) {
        super(message);
    }
}
