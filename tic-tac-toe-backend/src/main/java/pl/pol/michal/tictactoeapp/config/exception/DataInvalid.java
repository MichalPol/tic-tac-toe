package pl.pol.michal.tictactoeapp.config.exception;

public class DataInvalid extends Exception {
    public DataInvalid(String message) {
        super(message);
    }
}
