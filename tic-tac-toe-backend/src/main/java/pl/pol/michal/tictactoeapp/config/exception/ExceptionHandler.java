package pl.pol.michal.tictactoeapp.config.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandler {
    @org.springframework.web.bind.annotation.ExceptionHandler(NotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleNotFound(NotFound ex) {
        return ex.getMessage();
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(DataInvalid.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleDataInvalid(DataInvalid ex) {
        return ex.getMessage();
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(Conflict.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String handleAlreadyExists(Conflict ex) {
        return ex.getMessage();
    }
}
