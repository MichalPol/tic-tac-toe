package pl.pol.michal.tictactoeapp.config.exception;

public class NotFound extends Exception{
    public NotFound(String msg) {
    super(msg);
}
}
