package pl.pol.michal.tictactoeapp.dto.field;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FieldDto {
    private Long id;
    private Integer rowGameBoard;
    private Integer columnGameBoard;
    private String fieldStatus;
}
