package pl.pol.michal.tictactoeapp.dto.game;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pol.michal.tictactoeapp.dto.field.FieldDto;
import pl.pol.michal.tictactoeapp.model.field.Field;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GameDto {
    private Long gameId;
    private String playerFirstName;
    private String playerFirstMark;
    private String playerSecondName;
    private String playerSecondMark;
    private String gameState;
    private Integer sizeGame;
    private String lastMove;
    private List<FieldDto> fields;
}
