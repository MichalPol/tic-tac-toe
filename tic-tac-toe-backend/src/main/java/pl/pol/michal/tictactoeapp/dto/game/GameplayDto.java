package pl.pol.michal.tictactoeapp.dto.game;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pol.michal.tictactoeapp.model.game.GameState;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GameplayDto {
    private Long idGame;
    private LocalDateTime timeGame;
    private GameState statusGame;
}
