package pl.pol.michal.tictactoeapp.dto.game;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GenerateAIMoveDto {
    @NotNull
    private Integer gameId;
}
