package pl.pol.michal.tictactoeapp.dto.game;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pol.michal.tictactoeapp.dto.player.PlayerAndMark;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerMove {
    @NotNull
    private PlayerAndMark activePlayer;
    @NotNull
    private Long gameId;
    @NotNull
    private Long fieldId;
}
