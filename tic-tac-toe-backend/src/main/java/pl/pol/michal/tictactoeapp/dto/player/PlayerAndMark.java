package pl.pol.michal.tictactoeapp.dto.player;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerAndMark {
    private String playerName;
    private FieldMark fieldMark;
}
