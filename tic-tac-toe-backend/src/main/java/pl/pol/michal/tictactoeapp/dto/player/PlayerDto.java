package pl.pol.michal.tictactoeapp.dto.player;

import lombok.*;
import pl.pol.michal.tictactoeapp.dto.game.GameDto;
import pl.pol.michal.tictactoeapp.dto.game.GameplayDto;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerDto {
    private String name;
    private List<GameplayDto> gameList;
}
