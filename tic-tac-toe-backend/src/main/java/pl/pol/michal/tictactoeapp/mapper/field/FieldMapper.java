package pl.pol.michal.tictactoeapp.mapper.field;

import org.springframework.stereotype.Component;
import pl.pol.michal.tictactoeapp.dto.field.FieldDto;
import pl.pol.michal.tictactoeapp.model.field.Field;

@Component
public class FieldMapper {

    public FieldDto toDto(Field field) {
        return FieldDto.builder()
                .id(field.getId())
                .rowGameBoard(field.getRowGameBoard())
                .columnGameBoard(field.getColumnGameBoard())
                .fieldStatus(field.getFieldMark().name())
                .build();
    }
}
