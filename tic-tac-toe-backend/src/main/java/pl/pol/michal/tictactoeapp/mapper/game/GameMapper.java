package pl.pol.michal.tictactoeapp.mapper.game;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.dto.game.GameDto;
import pl.pol.michal.tictactoeapp.model.game.Game;
import pl.pol.michal.tictactoeapp.service.field.FieldService;

@Component
@RequiredArgsConstructor
public class GameMapper {

    private final FieldService fieldService;

    public GameDto toDto(Game game) throws DataInvalid {
        return GameDto.builder()
                .gameId(game.getId())
                .playerFirstName(game.getFirstPlayer().getName())
                .playerFirstMark(game.getFirstPlayerMark().name())
                .playerSecondName(game.getSecondPlayer().getName())
                .playerSecondMark(game.getSecondPlayerMark().name())
                .gameState(game.getGameState().name())
                .sizeGame(game.getGameBoard().getSizeBoard())
                .lastMove(game.getLastMove().name())
                .fields(fieldService.replaceFieldToFieldDto(game.getGameBoard().getListFieldStates()))
                .build();
    }
}
