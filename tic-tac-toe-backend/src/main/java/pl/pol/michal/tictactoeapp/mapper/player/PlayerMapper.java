package pl.pol.michal.tictactoeapp.mapper.player;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.pol.michal.tictactoeapp.dto.game.GameplayDto;
import pl.pol.michal.tictactoeapp.dto.player.PlayerDto;
import pl.pol.michal.tictactoeapp.model.game.Game;
import pl.pol.michal.tictactoeapp.model.player.Player;

import java.util.ArrayList;
import java.util.List;


@RequiredArgsConstructor
@Component
public class PlayerMapper {

    public PlayerDto toDto(Player player) {
        List<GameplayDto> gameplayDtoList = new ArrayList<>();
        List<Game> gameList = player.getGames();
        for (Game game : gameList) {
            gameplayDtoList.add(new GameplayDto(game.getId(), game.getCreateGameTime(), game.getGameState()));
        }
        return PlayerDto.builder()
                .name(player.getName())
                .gameList(gameplayDtoList)
                .build();
    }
}
