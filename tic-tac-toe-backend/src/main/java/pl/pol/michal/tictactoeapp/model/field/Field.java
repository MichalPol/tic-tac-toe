package pl.pol.michal.tictactoeapp.model.field;

import lombok.*;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;


import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@Entity(name = "fields")
public class Field {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Min(0)
    private Integer rowGameBoard;

    @NotNull
    @Min(0)
    private Integer columnGameBoard;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private FieldMark fieldMark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) throws DataInvalid {
        if (0 > id ){
            throw new DataInvalid("Id value is less than 0");
        }
        this.id = id;

    }

    public Integer getRowGameBoard() {
        return rowGameBoard;
    }

    public void setRowGameBoard(Integer rowGameBoard) throws DataInvalid {
        if (0 > rowGameBoard ){
            throw new DataInvalid("Row value is less than 0");
        }
        this.rowGameBoard = rowGameBoard;
    }

    public Integer getColumnGameBoard() {
        return columnGameBoard;
    }

    public void setColumnGameBoard(Integer columnGameBoard) throws DataInvalid {
        if (0 > columnGameBoard ){
            throw new DataInvalid("Column value is less than 0");
        }
        this.columnGameBoard = columnGameBoard;
    }

    public FieldMark getFieldMark() {
        return fieldMark;
    }

    public void setFieldMark(FieldMark fieldMark) throws DataInvalid {
        if (null == fieldMark){
            throw new DataInvalid("FieldMark is null");
        }
        this.fieldMark = fieldMark;
    }
}
