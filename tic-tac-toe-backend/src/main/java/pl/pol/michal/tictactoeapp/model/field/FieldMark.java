package pl.pol.michal.tictactoeapp.model.field;

public enum FieldMark {
    X,
    O,
    EMPTY
}
