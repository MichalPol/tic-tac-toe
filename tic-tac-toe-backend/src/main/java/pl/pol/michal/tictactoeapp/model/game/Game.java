package pl.pol.michal.tictactoeapp.model.game;


import lombok.*;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;
import pl.pol.michal.tictactoeapp.model.gameboard.GameBoard;
import pl.pol.michal.tictactoeapp.model.player.Player;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Builder
@Entity(name = "games")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private GameBoard gameBoard;

    @NotNull
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Player firstPlayer;

    private FieldMark firstPlayerMark;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Player secondPlayer;

    private FieldMark secondPlayerMark;

    @NotNull
    private LocalDateTime createGameTime;

    private LocalDateTime endGameTime;

    @Enumerated(value = EnumType.STRING)
    private GameState gameState;

    @Enumerated(value = EnumType.STRING)
    private FieldMark lastMove;

    public void setId(Long id) throws DataInvalid {
        if (id < 0){
            throw new DataInvalid("Id is Incorrect");
        }
        this.id = id;
    }

    public void setGameBoard(GameBoard gameBoard) {
        this.gameBoard = gameBoard;
    }

    public void setFirstPlayer(Player firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public void setFirstPlayerMark(FieldMark firstPlayerMark) throws DataInvalid {
        if (firstPlayerMark == FieldMark.EMPTY){
            throw new DataInvalid("Can't set an empty player");
        }
        this.firstPlayerMark = firstPlayerMark;
    }

    public void setSecondPlayer(Player secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    public void setSecondPlayerMark(FieldMark secondPlayerMark) throws DataInvalid {
        if (secondPlayerMark == FieldMark.EMPTY){
            throw new DataInvalid("Can't set an empty player");
        }
        this.secondPlayerMark = secondPlayerMark;
    }

    public void setCreateGameTime(LocalDateTime createGameTime) {
        this.createGameTime = createGameTime;
    }

    public void setEndGameTime(LocalDateTime endGameTime) throws DataInvalid {
        if (this.createGameTime.isAfter( endGameTime)){
            throw new DataInvalid("");
        }
        this.endGameTime = endGameTime;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public void setLastMove(FieldMark lastMove) throws DataInvalid {
        if (lastMove == FieldMark.EMPTY){
            throw new DataInvalid("Can't set an empty player");
        }
        this.lastMove = lastMove;
    }
}
