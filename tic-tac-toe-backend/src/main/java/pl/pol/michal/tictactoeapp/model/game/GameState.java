package pl.pol.michal.tictactoeapp.model.game;

public enum GameState {
    OPENED,
    FIRST_PLAYER_WIN,
    SECOND_PLAYER_WIN,
    DRAW
}
