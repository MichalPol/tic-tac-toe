package pl.pol.michal.tictactoeapp.model.gameboard;

import lombok.*;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.model.field.Field;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@Entity(name = "game_boards")
public class GameBoard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private List<Field> listFieldStates;

    @Min(3)
    @Max(10)
    @NotNull
    private Integer sizeBoard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) throws DataInvalid {
        if (0 > id){
            throw new DataInvalid("Id value is less than 0");
        }
        this.id = id;
    }

    public List<Field> getListFieldStates() {
        return listFieldStates;
    }

    public void setListFieldStates(List<Field> listFieldStates) throws DataInvalid {
        if (null == listFieldStates){
            throw new DataInvalid("List is null");
        }
        this.listFieldStates = listFieldStates;
    }

    public Integer getSizeBoard() {
        return sizeBoard;
    }

    public void setSizeBoard(Integer sizeBoard) throws DataInvalid {
        if (3 > sizeBoard){
            throw new DataInvalid("Size board  is less than 3");
        }
        this.sizeBoard = sizeBoard;
    }
}
