package pl.pol.michal.tictactoeapp.model.player;

import lombok.*;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.model.game.Game;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@Builder
@Entity(name = "players")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 2, max = 20)
    @Column(unique = true)
    private String name;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private List<Game> games;

    public void setId(Long id) throws DataInvalid {
        if (id < 0) {
            throw new DataInvalid("Id is less than 0");
        }
        this.id = id;
    }

    public void setName(String name) throws DataInvalid {
        if (null == name || name.length() < 2 || name.length() > 20) {
            throw new DataInvalid("Name player Incorrect");
        }
        this.name = name;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }
}
