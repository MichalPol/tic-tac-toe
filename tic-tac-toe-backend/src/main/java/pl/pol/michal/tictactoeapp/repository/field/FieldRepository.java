package pl.pol.michal.tictactoeapp.repository.field;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.pol.michal.tictactoeapp.model.field.Field;

public interface FieldRepository extends JpaRepository<Field, Long> {
}
