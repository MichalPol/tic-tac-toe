package pl.pol.michal.tictactoeapp.repository.game;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pol.michal.tictactoeapp.model.game.Game;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {
}
