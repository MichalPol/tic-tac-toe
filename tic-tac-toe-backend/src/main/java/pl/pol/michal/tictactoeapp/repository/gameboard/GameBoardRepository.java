package pl.pol.michal.tictactoeapp.repository.gameboard;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.pol.michal.tictactoeapp.model.gameboard.GameBoard;

public interface GameBoardRepository extends JpaRepository<GameBoard, Long> {
}
