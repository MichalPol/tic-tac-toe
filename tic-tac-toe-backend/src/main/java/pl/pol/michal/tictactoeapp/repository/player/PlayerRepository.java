package pl.pol.michal.tictactoeapp.repository.player;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pol.michal.tictactoeapp.model.player.Player;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long>{

    Optional<Player> findByName(@NotNull String name);

}
