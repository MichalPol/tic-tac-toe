package pl.pol.michal.tictactoeapp.service.field;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pol.michal.tictactoeapp.config.exception.Conflict;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.dto.field.FieldDto;
import pl.pol.michal.tictactoeapp.mapper.field.FieldMapper;
import pl.pol.michal.tictactoeapp.model.field.Field;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;
import pl.pol.michal.tictactoeapp.repository.field.FieldRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class FieldService {

    private final FieldRepository fieldRepository;
    private final FieldMapper fieldMapper;

    public Field checkField(Long fieldId, FieldMark fieldMark) throws NotFound, Conflict, DataInvalid {

        Optional<Field> optionalField = fieldRepository.findById(fieldId);

        if (fieldMark == FieldMark.EMPTY) {
            throw new DataInvalid("Cannot set an empty mark");
        }

        if (null == fieldMark) {
            throw new DataInvalid("FieldMark is null");
        }

        if (optionalField.isEmpty()) {
            throw new NotFound("Not Found Field");

        } else {
            Field field = optionalField.get();

            if (field.getFieldMark() == FieldMark.EMPTY) {
                field.setFieldMark(fieldMark);
                return field;
            } else {
                throw new Conflict("Field is checked");
            }
        }
    }

    public List<Field> getListEmptyFields(Integer sizeGameBoard) throws DataInvalid {

        if (null != sizeGameBoard && sizeGameBoard > 2) {
            List<Field> fieldList = new ArrayList<>();

            for (int i = 0; i < sizeGameBoard; i++) {

                for (int j = 0; j < sizeGameBoard; j++) {
                    Field field = Field.builder()
                            .id(null)
                            .columnGameBoard(i)
                            .rowGameBoard(j)
                            .fieldMark(FieldMark.EMPTY)
                            .build();
                    fieldList.add(field);
                }
            }
            return fieldRepository.saveAll(fieldList);
        }
        throw new DataInvalid("Game board must bigger that 2");
    }

    public List<FieldDto> replaceFieldToFieldDto(List<Field> fields) throws DataInvalid {
        if (null != fields && fields.size() != 0) {
            List<FieldDto> fieldDtoList = new ArrayList<>();
            for (Field field : fields) {
                fieldDtoList.add(fieldMapper.toDto(field));
            }
            return fieldDtoList;
        }
        throw new DataInvalid("List Fields is invalid");
    }

    public Field saveFieldToRepository(Field field) throws DataInvalid {
        if (null != field.getColumnGameBoard() && null != field.getRowGameBoard()){
            return fieldRepository.save(field);
        }
        throw new DataInvalid("Parameters are incorrect ");
    }

    @Transactional
    public void fieldDelete(List<Field> fieldsToDelete){
        fieldRepository.deleteAll(fieldsToDelete);
    }
}
