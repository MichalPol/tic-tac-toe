package pl.pol.michal.tictactoeapp.service.game;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.dto.game.GenerateAIMoveDto;
import pl.pol.michal.tictactoeapp.model.field.Field;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;
import pl.pol.michal.tictactoeapp.model.game.Game;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AIService {

    private final GameService gameService;

    @Transactional
    public Integer generateAIMove(GenerateAIMoveDto generateAIMoveDto) throws NotFound, DataInvalid {

        Game game = gameService.getGame(generateAIMoveDto.getGameId().longValue());

        //todo implement computer game strategies
        List<Field> fields = game.getGameBoard().getListFieldStates();
        for (int i = 0; i < fields.size(); i++) {
            if (fields.get(i).getFieldMark().equals(FieldMark.EMPTY)){
                return i;
            }
        }
        throw new DataInvalid("Data Invalid, empty fields");
    }

}
