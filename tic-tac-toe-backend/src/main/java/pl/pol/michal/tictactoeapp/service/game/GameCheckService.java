package pl.pol.michal.tictactoeapp.service.game;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.model.field.Field;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;

import java.util.List;

@RequiredArgsConstructor
@Service
public class GameCheckService {

    public boolean gameCheck(List<Field> listFieldStates, FieldMark playerMoveMark, Integer sizeBoard) throws DataInvalid {

        int iteration = 0;

        checkParameters(listFieldStates,playerMoveMark,sizeBoard);

        boolean result;
        result = checkFirstDiagonal(listFieldStates, playerMoveMark, sizeBoard);
        if (!result){
            result = checkSecondDiagonal(listFieldStates, playerMoveMark, sizeBoard);
        }

        if (!result){
            do {
                result = checkColumn(listFieldStates, playerMoveMark, iteration);
                if (!result){
                    result =  checkRow(listFieldStates,playerMoveMark, iteration);
                }
                iteration++;
            }
            while (!result && iteration < sizeBoard );

        }
        return result;
    }

    private void checkParameters(List<Field> listFieldStates, FieldMark playerMoveMark, Integer sizeBoard) throws DataInvalid {

        if (sizeBoard < 3 ) {
            throw new DataInvalid("Game board is small");
        }

        if (listFieldStates.size()  < 9 ) {
            throw new DataInvalid("incorrect number of playing fields");
        }

        if (playerMoveMark == FieldMark.EMPTY ) {
            throw new DataInvalid("field character is invalid, cannot be checked  EMPTY");
        }
    }

    private boolean checkColumn(List<Field> list, FieldMark fieldMark, int col){
        for (Field field : list) {
            if (field.getColumnGameBoard() == col) {
                if (field.getFieldMark() != fieldMark) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkRow(List<Field> list, FieldMark fieldMark, int row){
        for (Field field : list) {
            if (field.getRowGameBoard() == row) {
                if (field.getFieldMark() != fieldMark) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkFirstDiagonal(List<Field> list, FieldMark fieldMark, int sizeBoard){
        for (Field field : list) {
            for (int i = 0; i < sizeBoard; i++) {
                if (field.getColumnGameBoard() == i && field.getRowGameBoard() == i) {
                    if (field.getFieldMark() != fieldMark) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean checkSecondDiagonal(List<Field> list, FieldMark fieldMark, int sizeBoard){
        for (Field field : list) {
            for (int i = 0; i  < sizeBoard; i++) {
                if (field.getColumnGameBoard() == i && field.getRowGameBoard() == (sizeBoard - i - 1)) {
                    if (field.getFieldMark() != fieldMark) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean checkForADraw(List<Field> listFieldStates) {
        for (Field listFieldState : listFieldStates) {
            if (listFieldState.getFieldMark() == FieldMark.EMPTY) {
                return false;
            }
        }
        return true;
    }
}
