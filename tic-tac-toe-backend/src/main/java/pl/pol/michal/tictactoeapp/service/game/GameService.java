package pl.pol.michal.tictactoeapp.service.game;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.pol.michal.tictactoeapp.config.exception.Conflict;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.dto.game.CreateNewGameDto;
import pl.pol.michal.tictactoeapp.dto.game.GameDto;
import pl.pol.michal.tictactoeapp.dto.game.GameplayDto;
import pl.pol.michal.tictactoeapp.dto.game.PlayerMove;
import pl.pol.michal.tictactoeapp.dto.player.CreatePlayerDto;
import pl.pol.michal.tictactoeapp.dto.player.PlayerDto;
import pl.pol.michal.tictactoeapp.mapper.game.GameMapper;
import pl.pol.michal.tictactoeapp.model.field.Field;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;
import pl.pol.michal.tictactoeapp.model.game.Game;
import pl.pol.michal.tictactoeapp.model.game.GameState;
import pl.pol.michal.tictactoeapp.model.gameboard.GameBoard;
import pl.pol.michal.tictactoeapp.model.player.Player;
import pl.pol.michal.tictactoeapp.repository.game.GameRepository;
import pl.pol.michal.tictactoeapp.service.field.FieldService;
import pl.pol.michal.tictactoeapp.service.gameboard.GameBoardService;
import pl.pol.michal.tictactoeapp.service.player.PlayerService;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class GameService {

    private final GameBoardService gameBoardService;
    private final PlayerService playerService;
    private final FieldService fieldService;
    private final GameCheckService gameCheckService;
    private final GameMapper gameMapper;
    private final GameRepository gameRepository;


    @Transactional
    public GameDto createNewGame(CreateNewGameDto createNewGameDto) throws NotFound, DataInvalid, Conflict {

        preGamePlayerCheck(createNewGameDto);

        GameBoard gameBoard = gameBoardService.createNewGameBoard(createNewGameDto.getSizeGameBoard());
        Player firstPlayer = playerService.getPlayerByName(createNewGameDto.getFirstPlayer().getPlayerName());
        Player secondPlayer = prepareSecondPlayer(createNewGameDto.getSecondPlayer().getPlayerName());

        Game game = Game.builder()
                .id(null)
                .gameBoard(gameBoard)
                .firstPlayer(firstPlayer)
                .firstPlayerMark(createNewGameDto.getFirstPlayer().getFieldMark())
                .secondPlayer(secondPlayer)
                .secondPlayerMark(createNewGameDto.getSecondPlayer().getFieldMark())
                .createGameTime(LocalDateTime.now())
                .endGameTime(null)
                .gameState(GameState.OPENED)
                .lastMove(FieldMark.O)
                .build();
        Game save = gameRepository.save(game);

        playerService.addGameToPlayer(firstPlayer, save);
        playerService.addGameToPlayer(secondPlayer, save);
        return gameMapper.toDto(save);
    }

    private Player prepareSecondPlayer(String secondPlayerName) throws NotFound, Conflict, DataInvalid {
        Optional<Player> secondPlayer = Optional.ofNullable(playerService.getPlayerByName(secondPlayerName));

        if (secondPlayer.isEmpty()) {
            CreatePlayerDto secondNewPlayer = new CreatePlayerDto(secondPlayerName);
            secondPlayer = Optional.ofNullable(playerService.createPlayerModel(secondNewPlayer));
        }

        return secondPlayer.get();
    }

    private void preGamePlayerCheck(CreateNewGameDto createNewGameDto) throws Conflict {
        if (createNewGameDto.getFirstPlayer().getPlayerName().equals("COMPUTER")) {
            throw new Conflict("You cannot play as a computer");
        }

        if (createNewGameDto.getFirstPlayer().getPlayerName().equals(createNewGameDto.getSecondPlayer().getPlayerName())) {
            throw new Conflict("You can't play with yourself");
        }

        if (createNewGameDto.getFirstPlayer().getFieldMark().equals(createNewGameDto.getSecondPlayer().getFieldMark())) {
            throw new Conflict("Players cannot have the same Mark");
        }
    }

    @Transactional
    public GameDto getGameDto(Long gameId) throws NotFound, DataInvalid {
        Game game = gameRepository.findById(gameId).orElseThrow(() -> new NotFound("Not found game"));
        return gameMapper.toDto(game);
    }

    public Game getGame(Long gameId) throws NotFound {
        return gameRepository.findById(gameId).orElseThrow(() -> new NotFound("Not found id game"));
    }

    @Transactional
    public GameDto playerMove(PlayerMove playerMove) throws NotFound, Conflict, DataInvalid {

        Game game = getGame(playerMove.getGameId());
        checkGameIsOpen(game);

        if (playerMove.getActivePlayer().getFieldMark() == game.getLastMove()) {
            throw new Conflict("I'm sorry, it's not your move");
        }

        Field field = fieldService.checkField(playerMove.getFieldId(), playerMove.getActivePlayer().getFieldMark());
        fieldService.saveFieldToRepository(field);

        boolean gameIsWin = gameCheckService
                .gameCheck(
                        game.getGameBoard().getListFieldStates(),
                        playerMove.getActivePlayer().getFieldMark(),
                        game.getGameBoard().getSizeBoard()
                );

        if (gameIsWin) {
            game = setWinGame(game, playerMove);
        } else {
            game = setDraw(game);
        }
        game.setLastMove(playerMove.getActivePlayer().getFieldMark());
        Game save = gameRepository.save(game);
        return gameMapper.toDto(save);
    }

    public Game setWinGame(Game game, PlayerMove playerMove) throws DataInvalid {
        if (playerMove.getActivePlayer().getFieldMark() == FieldMark.EMPTY) {
            return game;
        }
        if (playerMove.getActivePlayer().getFieldMark() == FieldMark.X) {
            game.setGameState(GameState.FIRST_PLAYER_WIN);
        } else {
            game.setGameState(GameState.SECOND_PLAYER_WIN);
        }
        game.setEndGameTime(LocalDateTime.now());
        return game;
    }

    public Game setDraw(Game game) throws DataInvalid {
        boolean draw = gameCheckService.checkForADraw(game.getGameBoard().getListFieldStates());
        if (draw) {
            game.setGameState(GameState.DRAW);
            game.setEndGameTime(LocalDateTime.now());
            return game;
        }
        return game;
    }

    public void checkGameIsOpen(Game game) throws Conflict {
        if (game.getGameState() != GameState.OPENED) {
            throw new Conflict("The game is closed");
        }
    }

    public PlayerDto gameDelete(Integer id) throws NotFound{
        Game game = getGame(id.longValue());
        fieldService.fieldDelete(game.getGameBoard().getListFieldStates());
        gameBoardService.gameBoardDelete(game.getGameBoard().getId());
        playerService.removeGameFromPlayer(game);
        gameRepository.delete(game);
        return playerService.replacePlayerToPlayerDto(game.getFirstPlayer());
    }


}

