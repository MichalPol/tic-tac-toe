package pl.pol.michal.tictactoeapp.service.gameboard;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.model.field.Field;
import pl.pol.michal.tictactoeapp.model.gameboard.GameBoard;
import pl.pol.michal.tictactoeapp.repository.gameboard.GameBoardRepository;
import pl.pol.michal.tictactoeapp.service.field.FieldService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class GameBoardService {

    private final FieldService fieldService;
    private final GameBoardRepository gameBoardRepository;

    @Transactional
    public GameBoard createNewGameBoard(Integer sizeGameBoard) throws DataInvalid {
        if (null == sizeGameBoard) {
            throw new DataInvalid("The size of the game board is null!");
        }

        if (!boardSizeIsCorrect(sizeGameBoard )) {
            throw new DataInvalid("The size of the game board is not correct!");
        }
        List<Field> listEmptyFields = fieldService.getListEmptyFields(sizeGameBoard);
        GameBoard gameBoard = GameBoard
                .builder()
                .id(null)
                .listFieldStates(listEmptyFields)
                .sizeBoard(sizeGameBoard)
                .build();
        return gameBoardRepository.save(gameBoard);
    }

    private Boolean boardSizeIsCorrect(Integer sizeGameBoard) {
        return 2 < sizeGameBoard && sizeGameBoard < 101;
    }

    public void gameBoardDelete(Long gameBoardId) throws NotFound {
        Optional<GameBoard> gameBoard = gameBoardRepository.findById(gameBoardId);
        if (gameBoard.isEmpty()){
            throw new NotFound("Not found game board");
        }
        gameBoardRepository.delete(gameBoard.get());
    }
}
