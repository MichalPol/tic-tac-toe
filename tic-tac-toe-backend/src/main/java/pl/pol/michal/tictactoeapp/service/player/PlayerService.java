package pl.pol.michal.tictactoeapp.service.player;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pol.michal.tictactoeapp.config.exception.Conflict;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.dto.game.GameplayDto;
import pl.pol.michal.tictactoeapp.dto.player.CreatePlayerDto;
import pl.pol.michal.tictactoeapp.dto.player.PlayerDto;
import pl.pol.michal.tictactoeapp.mapper.player.PlayerMapper;
import pl.pol.michal.tictactoeapp.model.game.Game;
import pl.pol.michal.tictactoeapp.model.player.Player;
import pl.pol.michal.tictactoeapp.repository.player.PlayerRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PlayerService {
    private final PlayerRepository playerRepository;
    private final PlayerMapper playerMapper;

    @Transactional
    public List<PlayerDto> getAllPlayers() {
        return playerRepository
                .findAll()
                .stream()
                .map(playerMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    public PlayerDto getPlayerDtoByName(String playerName) throws NotFound {
        return playerRepository.findByName(playerName).map(playerMapper::toDto).orElseThrow(() -> new NotFound("Not found player: " + playerName));
    }

    @Transactional
    public Player getPlayerByName(String playerName) throws NotFound {
        return playerRepository.findByName(playerName).orElseThrow(() -> new NotFound("Not found player: " + playerName));
    }

    public PlayerDto replacePlayerToPlayerDto(Player player){
        return playerMapper.toDto(player);
    }

    @Transactional
    public PlayerDto createPlayer(CreatePlayerDto createPlayerDto) throws Conflict, DataInvalid {

        validatePlayerName(createPlayerDto.getName());
        Optional<Player> playerOptional = playerRepository.findByName(createPlayerDto.getName());

        if (playerOptional.isEmpty()) {
            Player player = Player.builder()
                    .id(null)
                    .name(createPlayerDto.getName())
                    .games(new ArrayList<>())
                    .build();

            Player save = playerRepository.save(player);
            return playerMapper.toDto(save);
        }
        throw new Conflict("Player is Exist");
    }

    @Transactional
    public Player createPlayerModel(CreatePlayerDto createPlayerDto) throws Conflict, DataInvalid {

        validatePlayerName(createPlayerDto.getName());
        Optional<Player> playerOptional = playerRepository.findByName(createPlayerDto.getName());
        if (playerOptional.isEmpty()) {
            Player player = Player.builder()
                    .id(null)
                    .name(createPlayerDto.getName())
                    .games(new ArrayList<>())
                    .build();

            return playerRepository.save(player);
        }
        throw new Conflict("Player is Exist");
    }

    private void validatePlayerName(String validatePlayerName) throws DataInvalid, Conflict {
        if (null == validatePlayerName){
            throw new DataInvalid("Player name is null");
        }

        if (validatePlayerName.equals("COMPUTER")){
            throw new Conflict("You cannot play as a computer");
        }
    }

    @Transactional
    public void addGameToPlayer(Player player, Game game) {
        List<Game> games = player.getGames();
        games.add(game);
        player.setGames(games);
        playerRepository.save(player);
    }

    @Transactional
    public void removeGameFromPlayer(Game game) {
        Player firstPlayer = game.getFirstPlayer();
        firstPlayer.getGames().remove(game);
        playerRepository.save(firstPlayer);

        Player secondPlayer = game.getSecondPlayer();
        secondPlayer.getGames().remove(game);
        playerRepository.save(secondPlayer);
    }

}
