package pl.pol.michal.tictactoeapp.web.controller.api.game;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.pol.michal.tictactoeapp.config.exception.Conflict;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.dto.game.CreateNewGameDto;
import pl.pol.michal.tictactoeapp.dto.game.GameDto;
import pl.pol.michal.tictactoeapp.dto.game.GenerateAIMoveDto;
import pl.pol.michal.tictactoeapp.dto.game.PlayerMove;
import pl.pol.michal.tictactoeapp.dto.player.PlayerDto;
import pl.pol.michal.tictactoeapp.service.game.AIService;
import pl.pol.michal.tictactoeapp.service.game.GameService;

import javax.transaction.Transactional;

@RequiredArgsConstructor
@RestController
@CrossOrigin
@RequestMapping("/api/game")
public class GameApi {

    private final GameService gameService;
    private final AIService aiService;

    @PostMapping
    public GameDto createNewGame(@RequestBody CreateNewGameDto createNewGameDto) throws NotFound, DataInvalid, Conflict {
        return gameService.createNewGame(createNewGameDto);
    }

    @PostMapping("/move")
    public GameDto playerMove(@RequestBody PlayerMove playerMove) throws Conflict, DataInvalid, NotFound {
        return gameService.playerMove(playerMove);
    }

    @GetMapping("/{id}")
    public GameDto getGame(@PathVariable Integer id) throws NotFound, DataInvalid {
        return gameService.getGameDto(id.longValue());
    }

    @PostMapping("/ai")
    public Integer generateAIMove(@RequestBody GenerateAIMoveDto gameBoardId) throws DataInvalid, NotFound {
        return aiService.generateAIMove(gameBoardId);
    }

    @Transactional
    @DeleteMapping("/{id}")
    public PlayerDto gameDelete(@PathVariable Integer id) throws NotFound, DataInvalid {
        return gameService.gameDelete(id);
    }
}
