package pl.pol.michal.tictactoeapp.web.controller.api.player;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.pol.michal.tictactoeapp.config.exception.Conflict;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.dto.player.CreatePlayerDto;
import pl.pol.michal.tictactoeapp.dto.player.PlayerDto;
import pl.pol.michal.tictactoeapp.service.player.PlayerService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/player")
@RequiredArgsConstructor
public class PlayerApi {

    private final PlayerService playerService;

    @GetMapping
    public List<PlayerDto> getAllPlayers(){
        return playerService.getAllPlayers();
    }

    @GetMapping("/{name}")
    public PlayerDto getPlayerName(@Valid @PathVariable String name) throws NotFound {
        return playerService.getPlayerDtoByName(name);
    }

    @PostMapping
    public PlayerDto createPlayer(@Valid @RequestBody CreatePlayerDto createPlayerDto) throws Conflict, DataInvalid {
        return playerService.createPlayer(createPlayerDto);
    }
}
