create table fields
(
    id                bigint       not null
        primary key,
    column_game_board int          not null,
    field_mark        varchar(255) not null,
    row_game_board    int          not null
);

create table game_boards
(
    id         bigint not null
        primary key,
    size_board int    not null
);

create table game_boards_list_field_states
(
    game_boards_id       bigint not null,
    list_field_states_id bigint not null,
    constraint UK_21s51v8jqlphqcvgk47k5pxi3
        unique (list_field_states_id),
    constraint FK7fljf2xawwtyqav2416x9qyba
        foreign key (game_boards_id) references game_boards (id),
    constraint FKily6y5tavma32y5i31qwtl1vd
        foreign key (list_field_states_id) references fields (id)
);

create table hibernate_sequence
(
    next_val bigint null
);

create table players
(
    id   bigint      not null
        primary key,
    name varchar(20) not null,
    constraint UK_pblmuavgrnr991e41662asko
        unique (name)
);

create table games
(
    id                 bigint       not null
        primary key,
    create_game_time   datetime     not null,
    end_game_time      datetime     null,
    first_player_mark  int          null,
    game_state         varchar(255) null,
    last_move          varchar(255) null,
    second_player_mark int          null,
    first_player_id    bigint       not null,
    game_board_id      bigint       null,
    second_player_id   bigint       null,
    constraint FK6xjbcjcnc474wsvlxv0fc5f1f
        foreign key (first_player_id) references players (id),
    constraint FKjcp0yllq52klw9f310iypnvde
        foreign key (second_player_id) references players (id),
    constraint FKt9r4d57wvcta92th0tba21dik
        foreign key (game_board_id) references game_boards (id)
);

create table players_games
(
    players_id bigint not null,
    games_id   bigint not null,
    constraint FK5kdd5ltvvq5etftesr8tvch1e
        foreign key (players_id) references players (id),
    constraint FKcke2f8afyh2ovddumih8nhhvy
        foreign key (games_id) references games (id)
);


