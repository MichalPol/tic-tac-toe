package pl.pol.michal.tictactoeapp.model.field;

import org.junit.jupiter.api.Test;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FieldTest {
    @Test
    void givenFieldWhenParameterCorrect() throws DataInvalid {
        //given
        Field field = new Field();
        //when
        field.setId(1L);
        field.setColumnGameBoard(0);
        field.setRowGameBoard(0);
        field.setFieldMark(FieldMark.EMPTY);

        //then
        assertEquals(1L,field.getId());
        assertEquals(0,field.getColumnGameBoard());
        assertEquals(0,field.getRowGameBoard());
        assertEquals(FieldMark.EMPTY,field.getFieldMark());
    }

    @Test
    void fieldDataInvalidWhenSetIdLessThanZero(){
        //given
        //when
        Field field = Field.builder()
                .id(1L)
                .columnGameBoard(1)
                .rowGameBoard(1)
                .fieldMark(FieldMark.EMPTY)
                .build();
        //then
        assertThrows(DataInvalid.class,()-> field.setId(-1l));
    }

    @Test
    void fieldDataInvalidWhenSetColumnLessThanZero(){
        //given
        //when
        Field field = Field.builder()
                .id(1L)
                .columnGameBoard(1)
                .rowGameBoard(1)
                .fieldMark(FieldMark.EMPTY)
                .build();
        //then
        assertThrows(DataInvalid.class,()-> field.setColumnGameBoard(-1));
    }

    @Test
    void fieldDataInvalidWhenSetRowLessThanZero(){
        //given
        //when
        Field field = Field.builder()
                .id(1L)
                .columnGameBoard(1)
                .rowGameBoard(1)
                .fieldMark(FieldMark.EMPTY)
                .build();
        //then
        assertThrows(DataInvalid.class,()-> field.setRowGameBoard(-1));
    }

    @Test
    void fieldDataInvalidWhenArgumentNull(){
        //given
        //when
        Field field = Field.builder()
                .id(1L)
                .columnGameBoard(1)
                .rowGameBoard(1)
                .fieldMark(FieldMark.EMPTY)
                .build();
        //then
        assertThrows(DataInvalid.class,()-> field.setFieldMark(null));
    }
}
