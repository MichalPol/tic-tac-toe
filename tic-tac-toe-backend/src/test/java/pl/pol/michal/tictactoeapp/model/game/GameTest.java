package pl.pol.michal.tictactoeapp.model.game;

import org.junit.jupiter.api.Test;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;
import pl.pol.michal.tictactoeapp.model.gameboard.GameBoard;
import pl.pol.michal.tictactoeapp.model.player.Player;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class GameTest {

    @Test
    void givenGameWhenParametersIsCorrect() throws DataInvalid {
        //given
        Game game = new Game();
        LocalDateTime localDateTime = LocalDateTime.now();

        //when
        game.setId(1L);
        game.setGameBoard(new GameBoard(1L, new ArrayList<>(), 3));
        game.setFirstPlayer(new Player(1L, "Bob", new ArrayList<>()));
        game.setFirstPlayerMark(FieldMark.X);
        game.setSecondPlayer(new Player(2L, "Mick", new ArrayList<>()));
        game.setSecondPlayerMark(FieldMark.O);
        game.setCreateGameTime(localDateTime);
        game.setEndGameTime(localDateTime);
        game.setGameState(GameState.OPENED);
        game.setLastMove(FieldMark.O);

        //then
        assertEquals(1L, game.getId());
        assertEquals(3, game.getGameBoard().getSizeBoard());
        assertEquals(0, game.getGameBoard().getListFieldStates().size());
        assertEquals("Bob", game.getFirstPlayer().getName());
        assertEquals(FieldMark.X, game.getFirstPlayerMark());
        assertEquals("Mick", game.getSecondPlayer().getName());
        assertEquals(FieldMark.O, game.getSecondPlayerMark());
        assertEquals(localDateTime, game.getCreateGameTime());
        assertEquals(localDateTime, game.getEndGameTime());
        assertEquals(GameState.OPENED, game.getGameState());
        assertEquals(FieldMark.O, game.getLastMove());
    }

    @Test
    void givenGameWhenIdIsLessThan0ShouldByThrowDataInvalid() {
        //given
        Game game = new Game();

        //when
        //then
        assertThrows(DataInvalid.class, () -> game.setId(-1L));
    }

    @Test
    void givenGameWhenFirstPlayerMarkSetEmptyShouldByThrowDataInvalid(){
        //given
        Game game = new Game();

        //when
        //then
        assertThrows(DataInvalid.class, () -> game.setFirstPlayerMark(FieldMark.EMPTY));
    }

    @Test
    void givenGameWhenSecondPlayerMarkSetEmptyShouldByThrowDataInvalid(){
        //given
        Game game = new Game();

        //when
        //then
        assertThrows(DataInvalid.class, () -> game.setSecondPlayerMark(FieldMark.EMPTY));
    }

    @Test
    void givenGameWhenLastMoveSetEmptyShouldByThrowDataInvalid() throws DataInvalid {
        //given
        Game game = new Game();

        //when
        //then
        assertThrows(DataInvalid.class, () -> game.setLastMove(FieldMark.EMPTY));
    }


    @Test
    void givenGameWhenSetDateCorrect() throws DataInvalid {
        //given
        Game game = new Game();
        LocalDateTime createGame = LocalDateTime.now();
        LocalDateTime endGame = createGame.plusMinutes(3L);

        //when
        game.setCreateGameTime(createGame);
        game.setEndGameTime(endGame);

        //then
        assertEquals(createGame, game.getCreateGameTime());
        assertEquals(endGame, game.getEndGameTime());
    }

    @Test
    void givenGameWhenSetNullEndGameCorrect() {
        //given
        Game game = new Game();
        LocalDateTime createGame = LocalDateTime.now();

        //when
        game.setCreateGameTime(createGame);

        //then
        assertEquals(createGame, game.getCreateGameTime());
        assertNull(game.getEndGameTime());
    }


    @Test
    void givenGameWhenSetTimeEndGameBeforeCreateGameShouldByThrowDataInvalid() {
        //given
        Game game = new Game();
        LocalDateTime createGame = LocalDateTime.now();
        LocalDateTime endGame = createGame.minusMinutes(3L);

        //when
        game.setCreateGameTime(createGame);

        //then
        assertThrows(DataInvalid.class, () -> game.setEndGameTime(endGame));
    }

}
