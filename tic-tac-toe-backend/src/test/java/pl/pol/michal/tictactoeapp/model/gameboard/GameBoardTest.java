package pl.pol.michal.tictactoeapp.model.gameboard;

import org.junit.jupiter.api.Test;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GameBoardTest {

    @Test
    void createGameBoardWhenParameterIsCorrect() throws DataInvalid {
        //given
        GameBoard gameBoardTest = new GameBoard();
        //when
        gameBoardTest.setId(1l);
        gameBoardTest.setListFieldStates(new ArrayList<>());
        gameBoardTest.setSizeBoard(3);
        //then
        assertEquals(1l, gameBoardTest.getId());
        assertThat(gameBoardTest.getListFieldStates(), empty());
        assertEquals(3, gameBoardTest.getSizeBoard());
    }

    @Test
    void gameBoardDataInvalidWhenIdLessThan0() throws DataInvalid {
        //given
        GameBoard gameBoardTest = new GameBoard();
        //when
        gameBoardTest.setId(1l);
        gameBoardTest.setListFieldStates(new ArrayList<>());
        gameBoardTest.setSizeBoard(3);
        //then
        assertThrows(DataInvalid.class, () -> gameBoardTest.setSizeBoard(-1));
    }


    @Test
    void gameBoardDataInvalidWhenSizeLessThan3() throws DataInvalid {
        //given
        GameBoard gameBoardTest = new GameBoard();
        //when
        gameBoardTest.setId(1l);
        gameBoardTest.setListFieldStates(new ArrayList<>());
        gameBoardTest.setSizeBoard(3);
        //then
        assertThrows(DataInvalid.class, () -> gameBoardTest.setSizeBoard(2));
    }

    @Test
    void gameBoardDataInvalidWhenListIstNull(){
        //given
        GameBoard gameBoardTest = new GameBoard();
        //when

        //then
        assertThrows(DataInvalid.class, () -> gameBoardTest.setListFieldStates(null));
    }

    @Test
    void createGameBoardWhenIdIsLess0ShouldByThrowDataInvalid(){
        //given
        GameBoard gameBoardTest = new GameBoard();
        //when

        //then
        assertThrows(DataInvalid.class, () -> gameBoardTest.setId(-1L));
    }
}
