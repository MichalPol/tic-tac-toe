package pl.pol.michal.tictactoeapp.model.player;

import org.junit.jupiter.api.Test;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {

    @Test
    void givePlayerWhenParametersCorrectItShouldGivePlayer() throws DataInvalid {
        //given
        Player player = new Player();
        //when
        player.setId(1L);
        player.setName("Bob");
        player.setGames(new ArrayList<>());

        //then
        assertEquals(1L, player.getId());
        assertEquals("Bob", player.getName());
        assertEquals(0, player.getGames().size());
    }

    @Test
    void givePlayerWhenIdLessThan0ShouldByThrowDataInvalid(){
        //given
        Player player = new Player();
        //when

        //then
        assertThrows(DataInvalid.class, ()  -> player.setId(-1L));
    }

    @Test
    void givePlayerWhenNamePlayerIsNullShouldByThrowDataInvalid() {
        //given
        Player player = new Player();
        //when

        //then
        assertThrows(DataInvalid.class, ()  -> player.setName(null));
    }
    @Test
    void givePlayerWhenNamePlayerIsLongStringShouldByThrowDataInvalid() {
        //given
        Player player = new Player();
        //when

        //then
        assertThrows(DataInvalid.class, ()  -> player.setName("qwsqweq2e12dcer3e121e1e12"));
    }
    @Test
    void givePlayerWhenNamePlayerIsToShortStringShouldByThrowDataInvalid() {
        //given
        Player player = new Player();
        //when

        //then
        assertThrows(DataInvalid.class, ()  -> player.setName("q"));
    }
}
