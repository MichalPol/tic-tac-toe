package pl.pol.michal.tictactoeapp.service.field;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.pol.michal.tictactoeapp.config.exception.Conflict;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.dto.field.FieldDto;
import pl.pol.michal.tictactoeapp.mapper.field.FieldMapper;
import pl.pol.michal.tictactoeapp.model.field.Field;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;
import pl.pol.michal.tictactoeapp.repository.field.FieldRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FieldServiceTest {

    @Mock
    FieldRepository fieldRepositoryMock;

    @Mock
    FieldMapper fieldMapper;

    @InjectMocks
    FieldService fieldServiceMock;


    private List<Field> prepareListEmptyFields() {
        Field field1 = new Field(1L, 0, 0, FieldMark.EMPTY);
        Field field2 = new Field(2L, 0, 1, FieldMark.EMPTY);
        Field field3 = new Field(3L, 0, 2, FieldMark.EMPTY);
        Field field4 = new Field(4L, 1, 0, FieldMark.EMPTY);
        Field field5 = new Field(5L, 1, 1, FieldMark.EMPTY);
        Field field6 = new Field(6L, 1, 2, FieldMark.EMPTY);
        Field field7 = new Field(7L, 2, 0, FieldMark.EMPTY);
        Field field8 = new Field(8L, 2, 1, FieldMark.EMPTY);
        Field field9 = new Field(9L, 2, 2, FieldMark.EMPTY);
        return Arrays.asList(field1, field2, field3, field4, field5, field6, field7, field8, field9);
    }

    @Test
    void checkFieldShouldByCorrect() throws Conflict, DataInvalid, NotFound {

        //given
        Field field = new Field(1L, 0, 0, FieldMark.EMPTY);
        when(fieldRepositoryMock.findById(1L)).thenReturn(Optional.of(field));

        //when
        Field fieldTest = fieldServiceMock.checkField(1L, FieldMark.O);

        //then
        assertEquals(FieldMark.O, fieldTest.getFieldMark());
    }

    @Test
    void checkFieldWhenFiledIsNotEmptyShouldByThrowConflict() {
        //given
        Field field = new Field(1L, 0, 0, FieldMark.O);
        //when
        when(fieldRepositoryMock.findById(1L)).thenReturn(java.util.Optional.of(field));

        //then
        assertThrows(Conflict.class, () -> fieldServiceMock.checkField(1L, FieldMark.X));
    }

    @Test
    void checkFieldWhenFieldNotFoundShouldByThrowNotFound() {
        //given
        //when
        when(fieldRepositoryMock.findById(-1L)).thenReturn(Optional.empty());

        //then
        assertThrows(NotFound.class, () -> fieldServiceMock.checkField(-1L, FieldMark.X));
    }

    @Test
    void checkFieldWhenMarkIsNullShouldByThrowDataInvalid() {
        //given
        //when
        //then
        assertThrows(DataInvalid.class, () -> fieldServiceMock.checkField(1L, null));
    }

    @Test
    void checkFieldWhenSetMarkEmptyShouldByThrowDataInvalid() {
        //given
        Field field = new Field(1L, 0, 0, FieldMark.EMPTY);
        //when
        when(fieldRepositoryMock.findById(1L)).thenReturn(java.util.Optional.of(field));

        //then
        assertThrows(DataInvalid.class, () -> fieldServiceMock.checkField(1L, FieldMark.EMPTY));
    }

    @Test
    void getListEmptyFieldsShouldByCorrect() throws DataInvalid {
        //given
        Field field1 = new Field(null, 0, 0, FieldMark.EMPTY);
        Field field2 = new Field(null, 0, 1, FieldMark.EMPTY);
        Field field3 = new Field(null, 0, 2, FieldMark.EMPTY);
        Field field4 = new Field(null, 1, 0, FieldMark.EMPTY);
        Field field5 = new Field(null, 1, 1, FieldMark.EMPTY);
        Field field6 = new Field(null, 1, 2, FieldMark.EMPTY);
        Field field7 = new Field(null, 2, 0, FieldMark.EMPTY);
        Field field8 = new Field(null, 2, 1, FieldMark.EMPTY);
        Field field9 = new Field(null, 2, 2, FieldMark.EMPTY);
        List<Field> fieldList = Arrays.asList(field1, field2, field3, field4, field5, field6, field7, field8, field9);

        lenient().when(fieldRepositoryMock.saveAll(fieldList)).thenReturn(prepareListEmptyFields());
        when(fieldServiceMock.getListEmptyFields(3)).thenReturn(prepareListEmptyFields());

        //when
        List<Field> listEmptyFieldsTest = fieldServiceMock.getListEmptyFields(3);

        //then
        assertThat(listEmptyFieldsTest.size(), is(9));
    }

    @Test
    void getListEmptyFieldsWhenSizeGameBoardIsLess3ShouldByThrowDataInvalid() {
        //given
        //when

        //then
        assertThrows(DataInvalid.class, () -> fieldServiceMock.getListEmptyFields(2));
    }

    @Test
    void getListEmptyFieldsWhenSizeGameBoardIsNullShouldByThrowDataInvalid(){
        //given
        //when

        //then
        assertThrows(DataInvalid.class, () -> fieldServiceMock.getListEmptyFields(null));
    }

    @Test
    void replaceFieldToFieldDtoWhenIsCorrect() throws DataInvalid {
        //given
        List<FieldDto> fieldDtoList = fieldServiceMock.replaceFieldToFieldDto(prepareListEmptyFields());
        //when
        //then
        assertEquals(9,fieldDtoList.size());
    }

    @Test
    void replaceFieldToFieldDtoWhenNullShouldByThrowDataInvalid(){
        //given
        //when
        //then
        assertThrows(DataInvalid.class,() -> fieldServiceMock.replaceFieldToFieldDto(null) );
    }

    @Test
    void replaceFieldToFieldDtoWhenEmptyListShouldByThrowDataInvalid(){
        //given
        //when
        //then
        assertThrows(DataInvalid.class,() -> fieldServiceMock.replaceFieldToFieldDto(new ArrayList<>()) );
    }

    @Test
    void saveFieldShouldByIsCorrect() throws DataInvalid {
        //given
        Field prepareField = new Field(null, 0, 0, FieldMark.X);
        when(fieldRepositoryMock.save(prepareField)).thenReturn(new Field(1L, 0, 1, FieldMark.X));

        //when

        Field fieldTest = fieldServiceMock.saveFieldToRepository(prepareField);
        //then
        assertEquals(1L, fieldTest.getId());
        assertEquals(0, fieldTest.getRowGameBoard());
        assertEquals(1, fieldTest.getColumnGameBoard());
        assertEquals(FieldMark.X, fieldTest.getFieldMark());
    }

    @Test
    void saveFieldSWhenRowIsNullShouldByThrowDataInvalid() throws DataInvalid {
        //given
        Field prepareField = new Field(null, null, 3, FieldMark.X);
        //when
        //then
        assertThrows(DataInvalid.class, () -> fieldServiceMock.saveFieldToRepository(prepareField));
    }
    @Test
    void saveFieldSWhenColumnIsNullShouldByThrowDataInvalid() throws DataInvalid {
        //given
        Field prepareField = new Field(null, 3, null, FieldMark.X);
        //when
        //then
        assertThrows(DataInvalid.class, () -> fieldServiceMock.saveFieldToRepository(prepareField));
    }
}
