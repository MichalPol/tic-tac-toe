package pl.pol.michal.tictactoeapp.service.game;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.dto.game.GenerateAIMoveDto;
import pl.pol.michal.tictactoeapp.model.field.Field;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;
import pl.pol.michal.tictactoeapp.model.game.Game;
import pl.pol.michal.tictactoeapp.model.game.GameState;
import pl.pol.michal.tictactoeapp.model.gameboard.GameBoard;
import pl.pol.michal.tictactoeapp.model.player.Player;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AIServiceTest {

    @InjectMocks
    AIService aiService;

    @Mock
    GameService gameService;

    private List<Field> listGameBoard() {
        Field field1 = new Field(1L, 0, 0, FieldMark.EMPTY);
        Field field2 = new Field(2L, 0, 1, FieldMark.EMPTY);
        Field field3 = new Field(3L, 0, 2, FieldMark.EMPTY);
        Field field4 = new Field(4L, 1, 0, FieldMark.EMPTY);
        Field field5 = new Field(5L, 1, 1, FieldMark.EMPTY);
        Field field6 = new Field(6L, 1, 2, FieldMark.EMPTY);
        Field field7 = new Field(7L, 2, 0, FieldMark.EMPTY);
        Field field8 = new Field(8L, 2, 1, FieldMark.EMPTY);
        Field field9 = new Field(9L, 2, 2, FieldMark.EMPTY);
        return Arrays.asList(field1, field2, field3, field4, field5, field6, field7, field8, field9);
    }

    private Game prepareGame() {
        List<Field> fieldList = listGameBoard();
        fieldList.set(0, new Field(1L, 0, 0, FieldMark.X));
        GameBoard gameBoard = GameBoard.builder()
                .id(1L)
                .listFieldStates(fieldList)
                .sizeBoard(3)
                .build();

        return Game.builder()
                .id(1L)
                .gameBoard(gameBoard)
                .firstPlayer(new Player())
                .firstPlayerMark(FieldMark.X)
                .secondPlayer(new Player())
                .secondPlayerMark(FieldMark.O)
                .createGameTime(LocalDateTime.now())
                .endGameTime(null)
                .gameState(GameState.OPENED)
                .lastMove(FieldMark.X)
                .build();
    }

    @Test
    void generateAIMoveShouldByCorrect() throws NotFound, DataInvalid {
        //given
        GenerateAIMoveDto generateAIMoveDto = new GenerateAIMoveDto(1);
        Game game = prepareGame();

        //when
        when(gameService.getGame(generateAIMoveDto.getGameId().longValue())).thenReturn(game);
        Integer move = aiService.generateAIMove(generateAIMoveDto);

        //then
        assertEquals(1, move);
    }

    @Test
    void generateAIMoveWhenFieldIsOccupiedShouldByNextFreeField() throws DataInvalid, NotFound {
        //given
        GenerateAIMoveDto generateAIMoveDto = new GenerateAIMoveDto(1);
        Game game = prepareGame();

        List<Field> fieldList = listGameBoard();
        fieldList.set(0, new Field(1L, 0, 0, FieldMark.X));
        fieldList.set(1, new Field(2L, 1, 1, FieldMark.O));
        prepareGame().getGameBoard().setListFieldStates(fieldList);

        //when
        when(gameService.getGame(1L)).thenReturn(game);
        Integer move = aiService.generateAIMove(generateAIMoveDto);

        //then
        assertEquals(1, move);
    }


    @Test
    void generateAIMoveWhenGameIdNotFoundShouldByThrowNotFound() throws NotFound, DataInvalid {
        //given
        GenerateAIMoveDto generateAIMoveDto = new GenerateAIMoveDto(-1);

        //when
        when(gameService.getGame(generateAIMoveDto.getGameId().longValue())).thenThrow(NotFound.class);

        //then
        assertThrows(NotFound.class, () -> aiService.generateAIMove(generateAIMoveDto));
    }

    @Test
    void generateAIMoveWhenSetEmptyFieldsShouldByThrowDataInvalid() throws DataInvalid, NotFound {
        //given
        GenerateAIMoveDto generateAIMoveDto = new GenerateAIMoveDto(1);

        Game game = prepareGame();
        List<Field> listFieldStates = game.getGameBoard().getListFieldStates();
        for (Field listFieldState : listFieldStates) {
            listFieldState.setFieldMark(FieldMark.X);
        }

        //when
        when(gameService.getGame(generateAIMoveDto.getGameId().longValue())).thenReturn(game);

        //then
        assertThrows(DataInvalid.class, () -> aiService.generateAIMove(generateAIMoveDto));
    }

}
