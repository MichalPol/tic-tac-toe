package pl.pol.michal.tictactoeapp.service.game;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.model.field.Field;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class GameCheckServiceTest {


    private List<Field> listGameBoard() {
        Field field1 = new Field(1L, 0, 0, FieldMark.O);
        Field field2 = new Field(2L, 0, 1, FieldMark.O);
        Field field3 = new Field(3L, 0, 2, FieldMark.X);
        Field field4 = new Field(4L, 1, 0, FieldMark.X);
        Field field5 = new Field(5L, 1, 1, FieldMark.X);
        Field field6 = new Field(6L, 1, 2, FieldMark.O);
        Field field7 = new Field(7L, 2, 0, FieldMark.O);
        Field field8 = new Field(8L, 2, 1, FieldMark.X);
        Field field9 = new Field(9L, 2, 2, FieldMark.X);
        return Arrays.asList(field1, field2, field3, field4, field5, field6, field7, field8, field9);
    }

    @Test
    void checkColumnShouldByAccordingWin() throws DataInvalid {
        //given
        List<Field> fieldList = listGameBoard();
        fieldList.set(1, new Field(2L, 1, 2, FieldMark.X));
        GameCheckService gameCheckService = new GameCheckService();

        //when
        boolean check = gameCheckService.gameCheck(fieldList, FieldMark.X, 3);

        //then
        assertTrue(check);
    }

    @Test
    void checkRowShouldByAccordingWin() throws DataInvalid {
        //given
        List<Field> fieldList = listGameBoard();
        fieldList.set(1, new Field(3L, 0, 2, FieldMark.O));
        GameCheckService gameCheckService = new GameCheckService();

        //when
        boolean check = gameCheckService.gameCheck(fieldList, FieldMark.X, 3);

        //then
        assertTrue(check);
    }

    @Test
    void checkFirstDiagonalShouldByAccordingWin() throws DataInvalid {
        //given
        List<Field> fieldList = listGameBoard();
        fieldList.set(1, new Field(2L, 0, 1, FieldMark.X));
        fieldList.set(2, new Field(3L, 0, 2, FieldMark.O));
        fieldList.set(4, new Field(5L, 1, 1, FieldMark.O));
        GameCheckService gameCheckService = new GameCheckService();

        //when
        boolean check = gameCheckService.gameCheck(fieldList, FieldMark.O, 3);

        //then
        assertTrue(check);
    }

    @Test
    void checkSecondDiagonalShouldByAccordingWin() throws DataInvalid {
        //given
        List<Field> fieldList = listGameBoard();
        fieldList.set(0, new Field(1L, 0, 0, FieldMark.X));
        GameCheckService gameCheckService = new GameCheckService();

        //when
        boolean check = gameCheckService.gameCheck(fieldList, FieldMark.X, 3);

        //then
        assertTrue(check);
    }

    @Test
    void checkShouldNotBeWin() throws DataInvalid {
        //given
        List<Field> fieldList = listGameBoard();
        GameCheckService gameCheckService = new GameCheckService();

        //when
        boolean checkX = gameCheckService.gameCheck(fieldList, FieldMark.X, 3);
        boolean checkO = gameCheckService.gameCheck(fieldList, FieldMark.O, 3);

        //then
        assertFalse(checkX);
        assertFalse(checkO);
    }

    @Test
    void checkShouldByADraw() throws DataInvalid {
        //given
        List<Field> fieldList = listGameBoard();
        GameCheckService gameCheckService = new GameCheckService();

        //when
        boolean checkX = gameCheckService.gameCheck(fieldList, FieldMark.X, 3);
        boolean checkO = gameCheckService.gameCheck(fieldList, FieldMark.O, 3);

        //then
        assertFalse(checkX);
        assertFalse(checkO);
    }

    @Test
    void checkShouldByNotADrawWhenGameBoardIsNotFull() throws DataInvalid {
        //given
        List<Field> fieldList = listGameBoard();
        GameCheckService gameCheckService = new GameCheckService();
        fieldList.set(1, new Field(5L, 1, 1, FieldMark.EMPTY));

        //when
        boolean checkX = gameCheckService.gameCheck(fieldList, FieldMark.X, 3);
        boolean checkO = gameCheckService.gameCheck(fieldList, FieldMark.O, 3);

        //then
        assertFalse(checkX);
        assertFalse(checkO);
    }

    @Test
    void gameCheckSetSmallBoardShouldByThrowDataInvalid() {
        //given
        List<Field> fieldList = listGameBoard();
        GameCheckService gameCheckService = new GameCheckService();

        //when

        //then
        assertThrows(DataInvalid.class, () -> gameCheckService.gameCheck(fieldList, FieldMark.X, 0));
    }

    @Test
    void gameCheckSetSmallListThrowDataInvalid() {
        //given
        GameCheckService gameCheckService = new GameCheckService();

        //when
        List<Field> fieldList = new ArrayList<>();

        //then
        assertThrows(DataInvalid.class, () -> gameCheckService.gameCheck(fieldList, FieldMark.X, 3));
    }

    @Test
    void gameCheckWhenFieldMarkEmptyThrowDataInvalid() {
        //given
        List<Field> fieldList = listGameBoard();
        GameCheckService gameCheckService = new GameCheckService();

        //when

        //then
        assertThrows(DataInvalid.class, () -> gameCheckService.gameCheck(fieldList, FieldMark.EMPTY, 3));
    }

}
