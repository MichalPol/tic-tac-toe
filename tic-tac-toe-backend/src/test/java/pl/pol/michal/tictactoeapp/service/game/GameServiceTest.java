package pl.pol.michal.tictactoeapp.service.game;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.pol.michal.tictactoeapp.config.exception.Conflict;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.dto.field.FieldDto;
import pl.pol.michal.tictactoeapp.dto.game.CreateNewGameDto;
import pl.pol.michal.tictactoeapp.dto.game.GameDto;
import pl.pol.michal.tictactoeapp.dto.game.GameplayDto;
import pl.pol.michal.tictactoeapp.dto.game.PlayerMove;
import pl.pol.michal.tictactoeapp.dto.player.PlayerAndMark;
import pl.pol.michal.tictactoeapp.dto.player.PlayerDto;
import pl.pol.michal.tictactoeapp.mapper.game.GameMapper;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;
import pl.pol.michal.tictactoeapp.model.game.Game;
import pl.pol.michal.tictactoeapp.model.game.GameState;
import pl.pol.michal.tictactoeapp.model.gameboard.GameBoard;
import pl.pol.michal.tictactoeapp.model.player.Player;
import pl.pol.michal.tictactoeapp.repository.game.GameRepository;
import pl.pol.michal.tictactoeapp.repository.gameboard.GameBoardRepository;
import pl.pol.michal.tictactoeapp.service.field.FieldService;
import pl.pol.michal.tictactoeapp.service.gameboard.GameBoardService;
import pl.pol.michal.tictactoeapp.service.player.PlayerService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GameServiceTest {

    @InjectMocks
    GameService gameServiceMock;

    @Mock
    GameRepository gameRepository;

    @Mock
    GameBoardService gameBoardServiceMock;

    @Mock
    PlayerService playerServiceMock;

    @Mock
    GameMapper gameMapperMock;

    @Mock
    FieldService fieldServiceMock;

    @Mock
    GameCheckService gameCheckServiceMock;

    @Mock
    GameBoardRepository gameBoardRepositoryMock;


    private List<FieldDto> prepareListFieldsDto() {
        FieldDto field1 = new FieldDto(1L, 0, 0, FieldMark.EMPTY.name());
        FieldDto field2 = new FieldDto(2L, 0, 1, FieldMark.EMPTY.name());
        FieldDto field3 = new FieldDto(3L, 0, 2, FieldMark.EMPTY.name());
        FieldDto field4 = new FieldDto(4L, 1, 0, FieldMark.EMPTY.name());
        FieldDto field5 = new FieldDto(5L, 1, 1, FieldMark.EMPTY.name());
        FieldDto field6 = new FieldDto(6L, 1, 2, FieldMark.EMPTY.name());
        FieldDto field7 = new FieldDto(7L, 2, 0, FieldMark.EMPTY.name());
        FieldDto field8 = new FieldDto(8L, 2, 1, FieldMark.EMPTY.name());
        FieldDto field9 = new FieldDto(9L, 2, 2, FieldMark.EMPTY.name());
        return Arrays.asList(field1, field2, field3, field4, field5, field6, field7, field8, field9);
    }

    private Game prepareEmptyGame() {
        return Game.builder()
                .id(1L)
                .firstPlayer(new Player())
                .firstPlayerMark(FieldMark.X)
                .secondPlayer(new Player())
                .secondPlayerMark(FieldMark.O)
                .gameState(GameState.OPENED)
                .gameBoard(new GameBoard())
                .lastMove(FieldMark.O)
                .build();
    }

    private GameDto prepareGameDto() {
        return GameDto.builder()
                .gameId(1L)
                .playerFirstName("Bob")
                .playerFirstMark(FieldMark.X.name())
                .playerSecondName("Mick")
                .playerSecondMark(FieldMark.O.name())
                .gameState(GameState.OPENED.name())
                .sizeGame(3)
                .fields(prepareListFieldsDto())
                .lastMove(FieldMark.O.name())
                .build();
    }

    @Test
    void createNewGameShouldByCorrect() throws Conflict, DataInvalid, NotFound {
        //given
        CreateNewGameDto createNewGameDto =
                new CreateNewGameDto(3,
                        new PlayerAndMark("Bob", FieldMark.X),
                        new PlayerAndMark("Mick", FieldMark.O));

        when(playerServiceMock.getPlayerByName("Bob")).thenReturn(new Player(1L, "Bob", new ArrayList<>()));
        when(playerServiceMock.getPlayerByName("Mick")).thenReturn(new Player(2L, "Mick", new ArrayList<>()));
        when(gameRepository.save(any())).thenReturn(new Game());
        GameDto gameDto = prepareGameDto();
        when(gameMapperMock.toDto(any())).thenReturn(gameDto);

//        //when
        GameDto newGameTest = gameServiceMock.createNewGame(createNewGameDto);

        //then
        assertEquals(1L, newGameTest.getGameId());
        assertEquals("Bob", newGameTest.getPlayerFirstName());
        assertEquals(FieldMark.X.name(), newGameTest.getPlayerFirstMark());
        assertEquals("Mick", newGameTest.getPlayerSecondName());
        assertEquals(FieldMark.O.name(), newGameTest.getPlayerSecondMark());
        assertEquals(GameState.OPENED.name(), newGameTest.getGameState());
        assertEquals(3, newGameTest.getSizeGame());
        assertEquals(FieldMark.O.name(), newGameTest.getLastMove());
        assertEquals(9, newGameTest.getFields().size());
    }

    @Test
    void createNewGameWhenFirstPlayerNameIsCOMPUTERShouldByThrowConflict() {
        //given
        CreateNewGameDto createNewGameDto =
                new CreateNewGameDto(3,
                        new PlayerAndMark("COMPUTER", FieldMark.X),
                        new PlayerAndMark("Mick", FieldMark.O));
        //when

        //then
        assertThrows(Conflict.class, () -> gameServiceMock.createNewGame(createNewGameDto));
    }

    @Test
    void createNewGameWhenFirstPlayerAndSecondPlayerAreTheSameNameShouldByThrowConflict() {
        //given
        CreateNewGameDto createNewGameDto =
                new CreateNewGameDto(3,
                        new PlayerAndMark("Mick", FieldMark.X),
                        new PlayerAndMark("Mick", FieldMark.O));
        //when

        //then
        assertThrows(Conflict.class, () -> gameServiceMock.createNewGame(createNewGameDto));
    }

    @Test
    void createNewGameWhenFirstPlayerAndSecondPlayerAreTheSameMarkShouldByThrowConflict() {
        //given
        CreateNewGameDto createNewGameDto =
                new CreateNewGameDto(3,
                        new PlayerAndMark("Bob", FieldMark.X),
                        new PlayerAndMark("Mick", FieldMark.X));
        //when

        //then
        assertThrows(Conflict.class, () -> gameServiceMock.createNewGame(createNewGameDto));
    }

    @Test
    void getGameDtoWhenIdIsFindShouldByCorrect() throws DataInvalid, NotFound {
        //given
        Game game = prepareEmptyGame();
        GameDto gameDto = prepareGameDto();
        when(gameRepository.findById(1L)).thenReturn(java.util.Optional.ofNullable(game));
        when(gameMapperMock.toDto(game)).thenReturn(gameDto);

        //when
        GameDto gameDtoTest = gameServiceMock.getGameDto(1L);

        //then
        assertEquals(1L, gameDtoTest.getGameId());
        assertEquals("Bob", gameDtoTest.getPlayerFirstName());
        assertEquals(FieldMark.X.name(), gameDtoTest.getPlayerFirstMark());
        assertEquals("Mick", gameDtoTest.getPlayerSecondName());
        assertEquals(FieldMark.O.name(), gameDtoTest.getPlayerSecondMark());
        assertEquals(GameState.OPENED.name(), gameDtoTest.getGameState());
        assertEquals(3, gameDtoTest.getSizeGame());
        assertEquals(FieldMark.O.name(), gameDtoTest.getLastMove());
        assertEquals(9, gameDtoTest.getFields().size());
    }

    @Test
    void getGameDtoWhenIdNotFoundShouldByThrowNotFound() {
        //given
        //when
        when(gameRepository.findById(1234L)).thenReturn(Optional.empty());

        //then
        assertThrows(NotFound.class, () -> gameServiceMock.getGameDto(1234L));
    }

    @Test
    void getGameDtoWhenIdIsNullShouldByThrowNotFound() {
        //given
        //when
        when(gameRepository.findById(null)).thenReturn(Optional.empty());

        //then
        assertThrows(NotFound.class, () -> gameServiceMock.getGameDto(null));
    }

    @Test
    void getGameDtoWhenIdIsLessThanZeroShouldByThrowNotFound() {
        //given
        //when
        when(gameRepository.findById(-1L)).thenReturn(Optional.empty());

        //then
        assertThrows(NotFound.class, () -> gameServiceMock.getGameDto(-1L));
    }

    @Test
    void getGameWhenIdIsFindShouldByCorrect() throws NotFound {
        //given
        Game game = prepareEmptyGame();
        game.setFirstPlayer(new Player(1L, "Bob", new ArrayList<>()));
        game.setSecondPlayer(new Player(2L, "Mick", new ArrayList<>()));
        game.setGameBoard(new GameBoard(1L, new ArrayList<>(), 3));
        when(gameRepository.findById(1L)).thenReturn(java.util.Optional.of(game));

        //when
        Game gameTest = gameServiceMock.getGame(1L);

        //then
        assertEquals(1L, gameTest.getId());
        assertEquals("Bob", gameTest.getFirstPlayer().getName());
        assertEquals(FieldMark.X, gameTest.getFirstPlayerMark());
        assertEquals("Mick", gameTest.getSecondPlayer().getName());
        assertEquals(FieldMark.O, gameTest.getSecondPlayerMark());
        assertEquals(GameState.OPENED, gameTest.getGameState());
        assertEquals(FieldMark.O, gameTest.getLastMove());
        assertEquals(3, gameTest.getGameBoard().getSizeBoard());
    }

    @Test
    void getGameWhenIdNotFoundShouldByThrowNotFound() {
        //given
        //when
        when(gameRepository.findById(1234L)).thenReturn(Optional.empty());

        //then
        assertThrows(NotFound.class, () -> gameServiceMock.getGame(1234L));
    }

    @Test
    void getGameWhenIdIsNullShouldByThrowNotFound() {
        //given
        //when
        when(gameRepository.findById(null)).thenReturn(Optional.empty());

        //then
        assertThrows(NotFound.class, () -> gameServiceMock.getGame(null));
    }

    @Test
    void getGameWhenIdIsLessThanZeroShouldByThrowNotFound() {
        //given
        //when
        when(gameRepository.findById(-1L)).thenReturn(Optional.empty());

        //then
        assertThrows(NotFound.class, () -> gameServiceMock.getGame(-1L));
    }

    @Test
    void playerMoveShouldByCorrect() throws Conflict, DataInvalid, NotFound {
        //given

        Game game = prepareEmptyGame();

        PlayerMove playerMove = PlayerMove.builder()
                .gameId(1L)
                .activePlayer(new PlayerAndMark("Bob", FieldMark.X))
                .fieldId(1L)
                .build();

        when(gameRepository.findById(1L)).thenReturn(Optional.ofNullable(game));
        when(gameMapperMock.toDto(any())).thenReturn(prepareGameDto());
        //when
        GameDto gameDtoTest = gameServiceMock.playerMove(playerMove);
        // then
        assertEquals(1L, gameDtoTest.getGameId());
        assertEquals("Bob", gameDtoTest.getPlayerFirstName());
        assertEquals(FieldMark.X.name(), gameDtoTest.getPlayerFirstMark());
        assertEquals("Mick", gameDtoTest.getPlayerSecondName());
        assertEquals(FieldMark.O.name(), gameDtoTest.getPlayerSecondMark());
        assertEquals(GameState.OPENED.name(), gameDtoTest.getGameState());
        assertEquals(3, gameDtoTest.getSizeGame());
        assertEquals(FieldMark.O.name(), gameDtoTest.getLastMove());
        assertEquals(9, gameDtoTest.getFields().size());
    }

    @Test
    void playerMoveWhenConflictMoveGameShouldByThrowConflict() throws Conflict, DataInvalid, NotFound {
        //given

        Game game = prepareEmptyGame();
        PlayerMove playerMove = PlayerMove.builder()
                .gameId(1L)
                .activePlayer(new PlayerAndMark("Bob", FieldMark.O))
                .fieldId(1L)
                .build();

        //when
        when(gameRepository.findById(1L)).thenReturn(Optional.ofNullable(game));

        // then
        assertThrows(Conflict.class, () -> gameServiceMock.playerMove(playerMove));
    }

    @Test
    void checkGameIsOpenWhenGameIsStatusDRAWShouldByThrowConflict() {
        //given
        Game game = new Game();
        //when
        game.setGameState(GameState.DRAW);
        //then
        assertThrows(Conflict.class, () -> gameServiceMock.checkGameIsOpen(game));
    }

    @Test
    void checkGameIsOpenWhenGameIsStatusFIRST_PLAYER_WINShouldByThrowConflict() {
        //given
        Game game = new Game();
        //when
        game.setGameState(GameState.FIRST_PLAYER_WIN);
        //then
        assertThrows(Conflict.class, () -> gameServiceMock.checkGameIsOpen(game));
    }

    @Test
    void checkGameIsOpenWhenGameIsStatusSECOND_PLAYER_WINShouldByThrowConflict() {
        //given
        Game game = new Game();
        //when
        game.setGameState(GameState.SECOND_PLAYER_WIN);
        //then
        assertThrows(Conflict.class, () -> gameServiceMock.checkGameIsOpen(game));
    }

    @Test
    void setWinGameWhenFirstPlayerWinShouldByGameStateFIRST_PLAYER_WIN() throws DataInvalid {
        //given
        Game game = prepareEmptyGame();
        game.setCreateGameTime(LocalDateTime.now().minusMinutes(5L));

        PlayerMove playerMove = PlayerMove.builder().activePlayer(new PlayerAndMark("Bob", FieldMark.X)).gameId(1L).fieldId(1L).build();

        //when
        Game gameTest = gameServiceMock.setWinGame(game, playerMove);
        //then
        assertEquals(GameState.FIRST_PLAYER_WIN, gameTest.getGameState());
    }

    @Test
    void setWinGameWhenSecondPlayerWinShouldByGameStateSECOND_PLAYER_WIN() throws DataInvalid {
        //given
        Game game = prepareEmptyGame();
        game.setCreateGameTime(LocalDateTime.now().minusMinutes(5L));

        PlayerMove playerMove = PlayerMove.builder().activePlayer(new PlayerAndMark("Bob", FieldMark.O)).gameId(1L).fieldId(1L).build();

        //when
        Game gameTest = gameServiceMock.setWinGame(game, playerMove);
        //then
        assertEquals(GameState.SECOND_PLAYER_WIN, gameTest.getGameState());
    }

    @Test
    void setWinGameWhenPlayerHaveMarkEMPTYShouldByGameStateOPENED() throws DataInvalid {
        //given
        Game game = prepareEmptyGame();
        game.setCreateGameTime(LocalDateTime.now().minusMinutes(5L));

        PlayerMove playerMove = PlayerMove.builder().activePlayer(new PlayerAndMark("Bob", FieldMark.EMPTY)).gameId(1L).fieldId(1L).build();

        //when
        Game gameTest = gameServiceMock.setWinGame(game, playerMove);
        //then
        assertEquals(GameState.OPENED, gameTest.getGameState());
    }

    @Test
    void setDrawWhenCheckIsTrueShouldByGameStateDRAW() throws DataInvalid {
        //given
        Game game = prepareEmptyGame();
        game.setCreateGameTime(LocalDateTime.now().minusMinutes(5L));
        when(gameCheckServiceMock.checkForADraw(any())).thenReturn(true);

        //when
        Game gameTest = gameServiceMock.setDraw(game);

        //then
        assertEquals(GameState.DRAW, gameTest.getGameState());
    }

    @Test
    void setDrawWhenCheckIsFalseShouldByGameStateOPENED() throws DataInvalid {
        //given
        Game game = prepareEmptyGame();
        game.setCreateGameTime(LocalDateTime.now().minusMinutes(5L));
        when(gameCheckServiceMock.checkForADraw(any())).thenReturn(false);

        //when
        Game gameTest = gameServiceMock.setDraw(game);

        //then
        assertEquals(GameState.OPENED, gameTest.getGameState());
    }

    @Test
    void gameDeleteWhenEverythingIsGood() throws NotFound, DataInvalid {
        //given
        Game game = prepareEmptyGame();
        Player firstPlayer = new Player(1L, "Bob", Arrays.asList(new Game(), new Game()));
        Player secondPlayer = new Player(2L, "Mick", Arrays.asList(new Game(), new Game()));
        game.setFirstPlayer(firstPlayer);
        game.setSecondPlayer(secondPlayer);
        when(gameRepository.findById(any())).thenReturn(Optional.ofNullable(game));
        PlayerDto playerFirst = PlayerDto.builder()
                .name("Bob")
                .gameList(List.of(new GameplayDto()))
                .build();
        when(playerServiceMock.replacePlayerToPlayerDto(game.getFirstPlayer())).thenReturn(playerFirst);
        //when
        PlayerDto playerDto = gameServiceMock.gameDelete(123);
        //then
        assertEquals(1, playerDto.getGameList().size());
    }

    @Test
    void gameDeleteWhenGameIdNotFoundShouldByThrowNotFound() {
        //given
        when(gameRepository.findById(1234L)).thenReturn(Optional.empty());
        //when

        //then
        assertThrows(NotFound.class, () -> gameServiceMock.gameDelete(1234) );
    }
}
