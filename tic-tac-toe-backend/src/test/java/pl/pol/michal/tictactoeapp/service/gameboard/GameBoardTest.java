package pl.pol.michal.tictactoeapp.service.gameboard;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.model.field.Field;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;
import pl.pol.michal.tictactoeapp.model.gameboard.GameBoard;
import pl.pol.michal.tictactoeapp.repository.gameboard.GameBoardRepository;
import pl.pol.michal.tictactoeapp.service.field.FieldService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GameBoardTest {

    @Mock
    FieldService fieldService;

    @Mock
    GameBoardRepository gameBoardRepository;

    @InjectMocks
    GameBoardService gameBoardService;

    private List<Field> prepareListFields (){
        Field field1 = new Field(1L, 0, 0, FieldMark.EMPTY);
        Field field2 = new Field(2L, 0, 1, FieldMark.EMPTY);
        Field field3 = new Field(3L, 0, 2, FieldMark.EMPTY);
        Field field4 = new Field(4L, 1, 0, FieldMark.EMPTY);
        Field field5 = new Field(5L, 1, 1, FieldMark.EMPTY);
        Field field6 = new Field(6L, 1, 2, FieldMark.EMPTY);
        Field field7 = new Field(7L, 2, 0, FieldMark.EMPTY);
        Field field8 = new Field(8L, 2, 1, FieldMark.EMPTY);
        Field field9 = new Field(9L, 2, 2, FieldMark.EMPTY);
        return Arrays.asList(field1, field2, field3, field4, field5, field6, field7, field8, field9);
    }

    private GameBoard prepareGameBoard(int sizeGameBoard) {
        return  GameBoard
                .builder()
                .id(null)
                .listFieldStates(new ArrayList<>())
                .sizeBoard(sizeGameBoard)
                .build();
    }

    @Test
    void createNewGameBoardShouldByIsCorrect() throws DataInvalid {
        //given
        when(fieldService.getListEmptyFields(3)).thenReturn(prepareListFields());
        GameBoard gameBoard = prepareGameBoard(3);
        gameBoard.setListFieldStates(prepareListFields());

        GameBoard gameBoardReturn = GameBoard
                .builder()
                .id(1L)
                .listFieldStates(prepareListFields())
                .sizeBoard(3)
                .build();
        when(gameBoardRepository.save(gameBoard)).thenReturn(gameBoardReturn);

        //when
        GameBoard gameBoardTest = gameBoardService.createNewGameBoard(3);

        //then
        assertEquals(1L,gameBoardTest.getId());
        assertEquals(3,gameBoardTest.getSizeBoard());
        assertEquals(9,gameBoardTest.getListFieldStates().size());
    }

    @Test
    void createNewGameBoardWhenSizeIsBoardLess3ShouldByThrowDataInvalid()  {
        //then
        assertThrows(DataInvalid.class, () -> gameBoardService.createNewGameBoard(2));
    }

    @Test
    void createNewGameBoardWhenSizeBoardIsMoreThan100ShouldByThrowDataInvalid() {
        //then
        assertThrows(DataInvalid.class, () -> gameBoardService.createNewGameBoard(101));
    }

    @Test
    void createNewGameBoardWhenSizeBoardIsNullShouldByThrowDataInvalid() {
        //then
        assertThrows(DataInvalid.class, () -> gameBoardService.createNewGameBoard(null));
    }
}
