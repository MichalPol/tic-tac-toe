package pl.pol.michal.tictactoeapp.service.player;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.pol.michal.tictactoeapp.config.exception.Conflict;
import pl.pol.michal.tictactoeapp.config.exception.DataInvalid;
import pl.pol.michal.tictactoeapp.config.exception.NotFound;
import pl.pol.michal.tictactoeapp.dto.player.CreatePlayerDto;
import pl.pol.michal.tictactoeapp.dto.player.PlayerDto;
import pl.pol.michal.tictactoeapp.mapper.player.PlayerMapper;
import pl.pol.michal.tictactoeapp.model.field.FieldMark;
import pl.pol.michal.tictactoeapp.model.game.Game;
import pl.pol.michal.tictactoeapp.model.game.GameState;
import pl.pol.michal.tictactoeapp.model.gameboard.GameBoard;
import pl.pol.michal.tictactoeapp.model.player.Player;
import pl.pol.michal.tictactoeapp.repository.player.PlayerRepository;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PlayerServiceTest {

    @InjectMocks
    private PlayerService playerServiceMock;

    @Mock
    private PlayerRepository playerRepositoryMock;

    @Mock
    private PlayerMapper playerMapper;

    private List<Player> preparePlayerData() {

        Player player1 = Player.builder()
                .id(1L)
                .name("Bob")
                .games(new ArrayList<>())
                .build();

        Player player2 = Player.builder()
                .id(2L)
                .name("Jony")
                .games(new ArrayList<>())
                .build();
        return Arrays.asList(player1, player2);
    }

    @Test
    void getAllPlayersShouldByTwoPlayer() {

        //given
        lenient().when(playerRepositoryMock.findAll()).thenReturn(preparePlayerData());

        //when
        List<PlayerDto> playerDtoList = playerServiceMock.getAllPlayers();

        //then
        assertThat(playerDtoList, hasSize(2));
    }

    @Test
    void getAllPlayersShouldByEmptyList() {

        //given
        lenient().when(playerRepositoryMock.findAll()).thenReturn(Collections.emptyList());

        //when
        List<PlayerDto> playerDtoList = playerServiceMock.getAllPlayers();

        //then
        assertThat(playerDtoList, hasSize(0));
    }

    @Test
    void getPlayerByNameShouldByIsCorrect() throws NotFound {

        //given
        Player player = Player.builder().name("Bob").id(1l).games(new ArrayList<>()).build();
        when(playerRepositoryMock.findByName("Bob")).thenReturn(java.util.Optional.ofNullable(player));

        //when
        Player playerTest = playerServiceMock.getPlayerByName("Bob");

        //then
        assertEquals(1L, playerTest.getId());
        assertEquals("Bob", playerTest.getName());
        assertEquals(0, playerTest.getGames().size());
    }

    @Test
    void getPlayerByNameShouldByThrowNotFound() {

        //given
        when(playerRepositoryMock.findByName("Mark")).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(NotFound.class, () -> playerServiceMock.getPlayerByName("Mark"));
    }

    @Test
    void getPlayerByNameDtoShouldByIsCorrect() throws NotFound {

        //given
        Player player = Player.builder().name("Bob").id(1L).games(new ArrayList<>()).build();
        when(playerRepositoryMock.findByName("Bob")).thenReturn(java.util.Optional.ofNullable(player));
        assert player != null;
        when(playerMapper.toDto(player)).thenReturn(PlayerDto.builder().name("Bob").gameList(new ArrayList<>()).build());

        //when
        PlayerDto playerTest = playerServiceMock.getPlayerDtoByName("Bob");

        //then
        assertEquals("Bob", playerTest.getName());
        assertEquals(0, playerTest.getGameList().size());
    }

    @Test
    void getPlayerByNameDtoShouldByThrowNotFound() {

        //given
        when(playerRepositoryMock.findByName("Mark")).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(NotFound.class, () -> playerServiceMock.getPlayerDtoByName("Mark"));
    }

    @Test
    void createPlayerModelShouldByIsCorrect() throws Conflict, DataInvalid {
        //given
        CreatePlayerDto createPlayerDto = new CreatePlayerDto("Jonatan");
        Player player = new Player(null, "Jonatan", new ArrayList<>());
        Player playerReturn = new Player(1L, "Jonatan", new ArrayList<>());
        when(playerRepositoryMock.save(player)).thenReturn(playerReturn);

        //when
        Player playerDtoTest = playerServiceMock.createPlayerModel(createPlayerDto);

        //then
        assertEquals(1L, playerDtoTest.getId());
        assertEquals("Jonatan", playerDtoTest.getName());
        assertEquals(0, playerDtoTest.getGames().size());
    }

    @Test
    void createPlayerModelWhenPlayerIsComputerShouldByThrowConflict() {
        //given
        CreatePlayerDto createPlayerDto = new CreatePlayerDto("COMPUTER");

        //when
        //then
        assertThrows(Conflict.class, () -> playerServiceMock.createPlayerModel(createPlayerDto));
    }

    @Test
    void createPlayerModelWhenExistsPlayerShouldByThrowConflict() {
        //given
        CreatePlayerDto createPlayerDto = new CreatePlayerDto("Jonatan");
        Player playerReturn = new Player(1L, "Jonatan", new ArrayList<>());
        when(playerRepositoryMock.findByName("Jonatan")).thenReturn(Optional.of(playerReturn));

        //when

        //then
        assertThrows(Conflict.class, () -> playerServiceMock.createPlayerModel(createPlayerDto));
    }

    @Test
    void createPlayerModelWhenPlayerIsNullShouldByThrowConflict() {
        //given
        CreatePlayerDto createPlayerDto = new CreatePlayerDto(null);

        //when
        //then
        assertThrows(DataInvalid.class, () -> playerServiceMock.createPlayerModel(createPlayerDto));
    }


    @Test
    void createPlayerShouldByIsCorrect() throws Conflict, DataInvalid {
        //given
        CreatePlayerDto createPlayerDto = new CreatePlayerDto("Jonatan");
        Player player = new Player(null, "Jonatan", new ArrayList<>());
        Player playerReturn = new Player(1L, "Jonatan", new ArrayList<>());
        PlayerDto playerDto = new PlayerDto("Jonatan", new ArrayList<>());
        when(playerRepositoryMock.save(player)).thenReturn(playerReturn);
        when(playerMapper.toDto(playerReturn)).thenReturn(playerDto);
        //when

        PlayerDto playerDtoTest = playerServiceMock.createPlayer(createPlayerDto);

        //then
        assertEquals("Jonatan", playerDtoTest.getName());
        assertEquals(0, playerDtoTest.getGameList().size());
    }

    @Test
    void createPlayerWhenPlayerIsComputerShouldByThrowConflict() {
        //given
        CreatePlayerDto createPlayerDto = new CreatePlayerDto("COMPUTER");

        //when
        //then
        assertThrows(Conflict.class, () -> playerServiceMock.createPlayer(createPlayerDto));
    }

    @Test
    void createPlayerWhenExistsPlayerShouldByThrowConflict() {
        //given
        CreatePlayerDto createPlayerDto = new CreatePlayerDto("Jonatan");
        Player playerReturn = new Player(1L, "Jonatan", new ArrayList<>());
        when(playerRepositoryMock.findByName("Jonatan")).thenReturn(Optional.of(playerReturn));

        //when
        //then
        assertThrows(Conflict.class, () -> playerServiceMock.createPlayer(createPlayerDto));
    }

    @Test
    void createPlayerWhenPlayerIsNullShouldByThrowConflict() {
        //given
        CreatePlayerDto createPlayerDto = new CreatePlayerDto(null);

        //when
        //then
        assertThrows(DataInvalid.class, () -> playerServiceMock.createPlayer(createPlayerDto));
    }

    @Test
    void addGameToPlayerWhenSuccessAdded() {
        //given
        Player player = new Player(1L, "Bob", new ArrayList<>());

        Game game = new Game();
        PlayerService playerService = mock(PlayerService.class);

        //when
        playerService.addGameToPlayer(player, game);

        //then
        verify(playerService, times(1)).addGameToPlayer(player, game);
    }

    @Test
    void replacePlayerToPlayerDtoShouldByCorrect(){
        //given
        Game gameBob = Game.builder()
                .id(1L)
                .firstPlayer(new Player(1L, "Bob", new ArrayList<>()))
                .firstPlayerMark(FieldMark.X)
                .secondPlayer(new Player())
                .secondPlayerMark(FieldMark.O)
                .gameState(GameState.OPENED)
                .gameBoard(new GameBoard())
                .lastMove(FieldMark.O)
                .build();
        Player player = Player.builder()
                .id(1L)
                .name("Bob")
                .games(List.of(gameBob))
                .build();

        when(playerMapper.toDto(player)).thenCallRealMethod();

        //when
        PlayerDto playerDto = playerServiceMock.replacePlayerToPlayerDto(player);

        //then
        assertEquals("Bob", playerDto.getName());
        assertEquals(1, playerDto.getGameList().size());
    }

    @Test
    void replacePlayerToPlayerDtoWhenPlayerHaveEmptyGameListShouldByEmptyGameList(){

        //given
        Player player = Player.builder()
                .id(1L)
                .name("Bob")
                .games(new ArrayList<>())
                .build();

        when(playerMapper.toDto(player)).thenCallRealMethod();

        //when
        PlayerDto playerDto = playerServiceMock.replacePlayerToPlayerDto(player);

        //then
        assertEquals("Bob", playerDto.getName());
        assertEquals(0, playerDto.getGameList().size());
    }
}
