import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {MenuGameComponent} from "./menu-game/menu-game.component";
import {StartPageComponent} from "./start-page/start-page.component";
import {GameComponent} from "./game/game.component";

const routes: Routes = [
  {path:'menu', component : MenuGameComponent},
  {path:'game', component : GameComponent},
  {path:'', component : StartPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule{}
