import {Component, DoCheck, OnInit} from '@angular/core';
import {Field} from "../models/Field";
import {GameService} from "../services/game.service";
import {Router} from "@angular/router";
import {Mark, PlayerAndMark} from "../models/CreateNewGame";
import {PlayerMove} from "../models/Game";
import {HttpGameService} from "../services/http-game.service";
import {Observable, of} from "rxjs";


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, DoCheck {

  gameId: number = 1;

  firstPlayer: string;
  firstMark: string;
  secondPlayer: string;
  secondMark: string;
  lastMove: string;
  sizeBoard: number;
  gameBoard: Field[];
  gameBoard$:Observable<Field[]> = new Observable();
  gameState: string;

  moveIA: boolean = true;


  constructor(private gameService: GameService, private router: Router, private http: HttpGameService) {
    if (this.gameService.game$ === undefined) {
      this.router.navigate(['/']);
    }
  }

  ngDoCheck(): void {
    if (this.secondPlayer == 'COMPUTER' && this.lastMove == 'X' && this.secondPlayer != undefined && this.lastMove != undefined) {
      if (this.moveIA) {
        setTimeout(() => this.generateAIMove(), 500);
        this.moveIA = false
      }
    }
  }


  ngOnInit(): void {
    this.gameService.game$.subscribe(game => {
      this.gameId = game.gameId
      this.firstPlayer = game.playerFirstName
      this.firstMark = game.playerFirstMark
      this.secondPlayer = game.playerSecondName
      this.secondMark = game.playerSecondMark
      this.lastMove = game.lastMove
      this.sizeBoard = game.sizeGame
      this.gameBoard = game.fields
      this.gameState = game.gameState
      this.gameBoard$ = of(game.fields)

    })
  }

  checkField(index: number): void {

    if (this.gameState === "OPENED") {
      if (this.gameBoard[index].fieldMark === undefined) {
        if (this.lastMove === 'O') {
          this.lastMove = 'X'
          this.gameBoard[index].fieldMark = 'X'
        } else {
          this.lastMove = 'O'
          this.gameBoard[index].fieldMark = 'O'
        }
        this.checkWin(index, this.lastMove)
        if (this.lastMove == 'X') {
          this.moveIA = true;
        }
      }

    }


  }

  checkWin(indexField: number, lastMove: string) {

    let checkPlayer: PlayerAndMark;
    if (lastMove === 'X') {
      checkPlayer = {
        playerName: this.firstPlayer,
        fieldMark: Mark.X
      }
    } else {
      checkPlayer = {
        playerName: this.secondPlayer,
        fieldMark: Mark.O
      }
    }
    let playerMove: PlayerMove = {
      activePlayer: checkPlayer,
      gameId: this.gameId,
      fieldId: this.gameBoard[indexField].id
    }

    this.http.playerMove(playerMove).subscribe(value => {

      this.gameState = value.gameState

    })

  }

  endGame(): void {
    console.log("koniec gry")
  }

  pauseGame(): void {
    console.log("cost tam zapisane i zrobione")
  }

  playAgain(): void {
    console.log("jeszcze raz")
  }

  generateAIMove() {
    this.http.generateAIMove(this.gameId).subscribe(value => {
      this.checkField(value)
    })
  }
}


