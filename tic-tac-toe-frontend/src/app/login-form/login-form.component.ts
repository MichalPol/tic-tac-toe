import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {GameService} from "../services/game.service";


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginFormComponent implements OnInit {

  name:string;
  errorFlag:boolean;


  constructor(private gameService: GameService) { }

  ngOnInit(): void {
  }

  fetchPlayer(): void{

    if (this.name === undefined || this.name === '' || this.name === null){
      this.errorFlag = true
    } else {
      this.errorFlag = false
      this.gameService.fetchFirstPlayer(this.name)
    }

  }

}
