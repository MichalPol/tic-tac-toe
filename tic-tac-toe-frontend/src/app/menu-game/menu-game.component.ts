import {Component, OnInit} from '@angular/core';
import {Gameplay} from "../models/Gameplay";
import {GameService} from "../services/game.service";
import {HttpGameService} from "../services/http-game.service";
import {CreateNewGame, Mark, PlayerAndMark} from "../models/CreateNewGame";
import {Router} from "@angular/router";
import {CreatePlayer} from "../models/CreatePlayer";

@Component({
  selector: 'app-menu-game',
  templateUrl: './menu-game.component.html',
  styleUrls: ['./menu-game.component.scss'],
})
export class MenuGameComponent implements OnInit {

  firstPlayer: string;
  firstMark: string;
  gamePlays: Gameplay[];

  errorSizeBoard: boolean;
  errorName: boolean

  sizeGameBoard: number;
  secondPlayer: string;
  secondMark:string = "O";

  playerAI = 'COMPUTER'


  constructor(private gameService: GameService, private http: HttpGameService, private router: Router) {

  }

  ngOnInit(): void {
    let testName = this.gameService.firstPlayer
    if (testName === undefined) {
      this.router.navigate(['/']);
    } else {
      this.firstPlayer = testName;
      this.firstMark = this.gameService.firstPlayerMark;
      this.gamePlays = this.gameService.gamePlays;
      console.log(this.gamePlays)
    }
  }

  createNewGame(secondNamePlayer: string) {

    this.errorName = secondNamePlayer === undefined || secondNamePlayer === '';
    this.errorSizeBoard = this.sizeGameBoard === undefined || this.sizeGameBoard < 3;

    if (secondNamePlayer === this.playerAI){
      this.secondPlayer = this.playerAI
    }

    if (secondNamePlayer !== undefined && this.sizeGameBoard > 2) {

      this.getSecondPlayer(secondNamePlayer)

      let playerFirstAndMark: PlayerAndMark = {
        playerName: this.firstPlayer,
        fieldMark: Mark.X
      }

      let secondFirstAndMark: PlayerAndMark = {
        playerName: this.secondPlayer,
        fieldMark: Mark.O
      }

      let newGame: CreateNewGame = {
        sizeGameBoard: this.sizeGameBoard,
        firstPlayer: playerFirstAndMark,
        secondPlayer: secondFirstAndMark
      }
      this.gameService.createNewGame(newGame)
    }
  }

  getSecondPlayer(secondNamePlayer: string) {

    this.http.getPlayer(secondNamePlayer).subscribe(
      value => this.secondPlayer = value.name,
      error => {
        let create: CreatePlayer = {
          name: secondNamePlayer,
        }
        this.http.createPlayer(create).subscribe(value => this.secondPlayer = value.name,   error =>{
          this.router.navigate(['/'])
        })
      })
  }

  clearGame() {
    this.firstPlayer = null;
    this.firstMark = null;
    this.gamePlays = null;
    this.sizeGameBoard = null;
    this.secondPlayer = null;
  }

  continueGame(idGame: number) {
    console.log(idGame)
   this.gameService.continueGame(idGame);
  }

  gameDelete(idGame: number){
    this.http.deleteGame(idGame).subscribe(value => this.gamePlays = value.gameList);
  }
}
