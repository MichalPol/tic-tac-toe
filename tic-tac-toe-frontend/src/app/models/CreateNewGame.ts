export interface CreateNewGame {
  sizeGameBoard: number,
  firstPlayer: PlayerAndMark,
  secondPlayer: PlayerAndMark,
}

export interface PlayerAndMark {
  playerName: string,
  fieldMark: Mark
}

export enum Mark{
  X,
  O,
  EMPTY

}
