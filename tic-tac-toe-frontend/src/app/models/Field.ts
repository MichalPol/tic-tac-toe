export interface Field {
  id:number;
  columnGameBoard: number;
  rowGameBoard: number;
  fieldMark: string;
}
