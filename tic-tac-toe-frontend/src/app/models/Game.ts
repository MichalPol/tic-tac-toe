import {Field} from "./Field";
import {PlayerAndMark} from "./CreateNewGame";

export interface Game{
  gameId:number,
  playerFirstName:string,
  playerFirstMark:string,
  playerSecondName:string,
  playerSecondMark:string,
  gameState:string,
  sizeGame:number,
  lastMove:string,
  fields:Field[],
}

export interface PlayerMove{
  activePlayer: PlayerAndMark,
  gameId: number,
  fieldId:number
}
