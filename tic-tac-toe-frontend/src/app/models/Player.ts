import {Gameplay} from "./Gameplay";

export interface Player{
  name:string;
  gameList:Gameplay[];
}
