import {Field} from "./Field";
import {Gameplay} from "./Gameplay";

export const Fields: Field[] = [
  {
    id: 1,
    column: 0,
    row: 0,
    mark: 'EMPTY'
  },
  {
    id: 2,
    column: 0,
    row: 1,
    mark: 'EMPTY'
  },
  {
    id: 3,
    column: 0,
    row: 2,
    mark: 'EMPTY'
  },
  {
    id: 4,
    column: 1,
    row: 0,
    mark: 'EMPTY'
  },
  {
    id: 5,
    column: 1,
    row: 1,
    mark: 'EMPTY'
  },
  {
    id: 6,
    column: 1,
    row: 2,
    mark: 'EMPTY'
  },
  {
    id: 7,
    column: 2,
    row: 0,
    mark: 'EMPTY'
  },
  {
    id: 8,
    column: 2,
    row: 1,
    mark: 'EMPTY'
  },
  {
    id: 9,
    column: 2,
    row: 2,
    mark: 'EMPTY'
  },


];

export const GAMEPLAYS: Gameplay[] =  [
  {  idGame: 1,
    timeGame: '12.03.2021',
    statusGame: 'Done'
  },
  {  idGame: 2,
    timeGame: '12.04.2021',
    statusGame: 'Done'
  },
  {  idGame: 3,
    timeGame: '01.06.2021',
    statusGame: 'Opened'
  },
  {  idGame: 4,
    timeGame: '11.07.2021',
    statusGame: 'Done'
  }

];
