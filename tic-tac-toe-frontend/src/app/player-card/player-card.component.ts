import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-player-card',
  templateUrl: './player-card.component.html',
  styleUrls: ['./player-card.component.scss']
})
export class PlayerCardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input()
  namePlayer:string;
  @Input()
  markPlayer:string;
  @Input()
  movement:string;

}
