import {Injectable} from '@angular/core';
import {HttpGameService} from "./http-game.service";
import {Gameplay} from "../models/Gameplay";
import {Router} from '@angular/router';
import {CreatePlayer} from "../models/CreatePlayer";
import {CreateNewGame} from "../models/CreateNewGame";
import {map} from "rxjs/operators";
import {Game} from "../models/Game";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class GameService {
  firstPlayer: string;
  firstPlayerMark: string;
  gamePlays: Gameplay[];
  game$: Observable<Game>;

  constructor(private http: HttpGameService, private router: Router,) {
  }

  fetchFirstPlayer(nameNewPlayer: string) {
    this.http.getPlayer(nameNewPlayer).pipe(
      map(value => {
          this.firstPlayer = value.name;
          this.firstPlayerMark = 'X';
          this.gamePlays = value.gameList;
        }
      )).subscribe(value => console.log(),
      error => {
        this.createFirstNewPlayer(nameNewPlayer);
      },
      () => this.router.navigate(['/menu'])
    );
  }

  createFirstNewPlayer(namePlayer: string) {
    let create: CreatePlayer = {
      name: namePlayer,
    }
    this.http.createPlayer(create).pipe(
      map(value => {
          this.firstPlayer = value.name
          this.gamePlays = value.gameList;
          this.firstPlayerMark = 'X'
          this.router.navigate(['/menu'])
        }
      )).subscribe()
  }

  createNewGame(createNewGame: CreateNewGame) {
    let sub = this.http.createNewGame(createNewGame).pipe(
      source => this.game$ = source
    ).subscribe()
    this.router.navigate(['/game'])
    sub.unsubscribe()
  }

  continueGame(idGame: number) {
    let sub = this.http.getGame(idGame).pipe(source =>
      this.game$ = source
    ).subscribe()
    this.router.navigate(['/game'])
    sub.unsubscribe()
  }
}

