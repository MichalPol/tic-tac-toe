import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {map, tap} from "rxjs/operators";
import {CreatePlayer} from "../models/CreatePlayer";
import {Player} from "../models/Player";
import {Game, PlayerMove} from "../models/Game";
import {CreateNewGame} from "../models/CreateNewGame";


@Injectable({
  providedIn: 'root'
})
export class HttpGameService {

   private url = 'http://localhost:8080/api'

  constructor(private http: HttpClient) { }

  getPlayer(namePlayer: string) : Observable<Player>{
    return this.http.get<Player>(this.url + '/player/' +  namePlayer )
  }
  createPlayer(createPlayer: CreatePlayer): Observable<any>{
   return  this.http.post(this.url + '/player', createPlayer)
  }

  getGame(gameId: number) :Observable<Game>{
     return this.http.get<Game>(this.url + '/game/' + gameId ).pipe(tap(console.log))
  }

  createNewGame(createNewGame:CreateNewGame): Observable<Game>{
     return this.http.post<Game>(this.url + '/game/', createNewGame)
  }

  playerMove(playerMove: PlayerMove){
     return this.http.post<Game>(this.url + '/game/move', playerMove)
  }

  generateAIMove(gameId){
     let gameIdMessage = { gameId: gameId}
     return this.http.post<number>(this.url + '/game/ai', gameIdMessage)
  }

  deleteGame(gameId:number){
     return this.http.delete<Player>(this.url + '/game/' + gameId)
  }

}
